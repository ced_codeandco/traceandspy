<div class="col-lg-12">
    <h2>
        Parts Count</h2>
    <p>
        Our software is the only one in the world that reads in a PDF of a P&amp;ID and carries out a parts count delivering the results in a table within minutes.</p>
    <p>
        The application will process the drawings, determine and output a list of diagram components containing:</p>
    <ul class="about-list">
        <li>
            Component name &nbsp;</li>
        <li>
            Component list ID Number</li>
        <li>
            Stream ID</li>
        <li>
            Phase (Gas/Liquid)</li>
        <li>
            Pipe Size</li>
    </ul>
    <p>
        The output is a table of results available either .txt, .xls or in .PDF format . Accuracy will be &gt; 95%. A manual parts count in our experience is usually at 90% accuracy unless double-checked.</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_1.png" /></p>
    <h2>
        II.2. Limited reverse engineering of diagram</h2>
    <p>
        The software will associate each component with the corresponding connectors and determine the following:</p>
    <ul class="about-list">
        <li>
            Connector intersections</li>
        <li>
            Remote connections</li>
        <li>
            Gas/liquid connectors.</li>
    </ul>
    <p>
        This is done taking into account the following rules</p>
    <ul class="about-list">
        <li>
            List all level gauges directly attached to a tank (through a connector)</li>
        <li>
            Determine the 2 innermost connectors and the halfway point between them</li>
        <li>
            Any connector placed above that point is for gas. All others are for liquid. Exception:</li>
        <li>
            A connector that connects the upper area with the lower area of a tank is for gas.</li>
    </ul>
    <p>
        The software will also associate text tags with the corresponding connector or component. There are generally 4 types of text:</p>
    <ul class="about-list">
        <li>
            Contained within a diagram component</li>
        <li>
            Associated with a diagram component</li>
        <li>
            Placed nearby the diagram component</li>
        <li>
            Placed nearby an arrow connector that starts from a component</li>
        <li>
            Placed nearby more than one diagram components. If no other rules can be applied, the text will be associated with the closest diagram component (center to center)</li>
        <li>
            Associated with a connector</li>
        <li>
            Not associated with any element</li>
    </ul>
    <p>
        Text recognized includes</p>
    <ul class="about-list">
        <li>
            Any text associated with the inches sign (&rdquo;)<br />
            eg. 6&rdquo; or 3&quot;-P-120089-A2A-D</li>
        <li>
            Connectors (diamond with letter inside) as per example in Appendix B</li>
        <li>
            Circle with LG inside as per appendix C</li>
        <li>
            Stream number (inside the stream arrow) as in the figure below</li>
    </ul>
    <p>
        <img src="uploads/userfiles/images/parst_count_2.jpg" style="width: 50%;" /></p>
    <p>
        The software will calculate the diameter for each connector. This is done taking into account the text tags of connectors and reductions, as determined at point 2.</p>
    <p>
        There about 60 types of items that are recognized. The parts and their designs tabled below are recognized by our software as are their variants.</p>
    <table align="center" border="1" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Generic name</strong></p>
            </td>
            <td style="width:272px;">
                <p>
                    <strong>Component diagram</strong></p>
            </td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Centrifugal Compressor</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image1.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Reciprocating Compressors</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image2.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Centrifugal Pump</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image3.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Reciprocating Pump</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image4.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Gas Turbine</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image5.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        ( shell and Tube ) </strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image6.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        ( shell and Tube ) </strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image7.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        ( shell and Tube )</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image8.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        ( shell and Tube )</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image9.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        (plate)</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image10.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Heat Exchanger<br />
                        (plate)</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image11.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Fin Fan Coolers</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image12.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Filter</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image13.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Filter</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image14.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    &nbsp;</p>
                <p>
                    <strong>I</strong><strong>nstrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image15.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>I</strong><strong>nstrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image16.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>I</strong><strong>nstrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image17.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>I</strong><strong>nstrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image18.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>I</strong><strong>nstrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image19.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p>
                    <strong>Instrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image20.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Instrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image21.png" />
                <p align="center">
                    With any text in the circle</p>
            </td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Instrumentation</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image22.png" />
                <p align="center">
                    With any text in the circle</p>
            </td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
                <p align="center">
                    &nbsp;</p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image23.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image24.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image25.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image26.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image27.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image28.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image29.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image30.png" /></td>
        </tr>
        <!--</tbody>
</table>
<table align="center" border="1" cellpadding="0" cellspacing="0">
    <tbody>-->
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image31.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image32.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image33.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image34.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image35.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image36.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image37.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image38.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image39.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image40.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image41.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image42.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image43.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image44.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image45.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image46.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image47.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image48.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image49.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>VALVE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image50.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Orifice</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image51.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Rotameter</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image52.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>ESDV</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image53.png" />
                <p align="center">
                    Note text x may be the letter &ldquo;S&rdquo; or &ldquo;H&rdquo; or &ldquo;M&rdquo; or no text at all</p>
            </td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>vessel</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image54.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>FLANGE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image55.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>FLANGE</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image56.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>REDUCER</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image57.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Tank</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image58.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Tank</strong></p>
            </td>
            <td style="width:272px;">
                <img src="uploads/userfiles/images/about/image59.png" /></td>
        </tr>
        <tr>
            <td style="width:161px;">
                <p align="center">
                    <strong>Other Connection</strong></p>
            </td>
            <td style="width:272px;">
                <p align="center">
                    <strong>A break in the line not defined above and not an intersection of lines</strong></p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>
        &nbsp;</p>
    <p>
        Any of the items above can be rotated. Usually they are rotated either 90 180 270 degrees. But at times it can be any other angle such as 40 degrees.</p>
    <p>
        The software, will take into account sizes inferred from stream and other aspects such as a reducer. For example the figure below shows a highlighted a portion of a line which is called 12&quot;-P-110012-A2A-D.</p>
    <p>
        In this figure we have a line that is 12&quot; but the first item is a reducer. The shape of item A is a trapezoid. It has a wide edge at the bottom and a narrow edge at the top.</p>
    <p>
        When the software encounters a reducer like item A, it means that the line is changing in diameter. So item A can be tagged with diameter 12&quot;. But everything past the narrow side of the trapezoid will be tagged with different diameter.</p>
    <p>
        Therefore item B and item C are to be tagged with a different diameter. You can see the 3/4&quot; near item C. This means that both item B and Item C as well as all other items on the line past the narrow end of the trapezoid of item A will be tagged with 3/4&quot;.</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_3.png" /></p>
    <p>
        The software also connects relevant parts of the diagram. Items A1 and A2 are diamonds with the letter &quot;I&quot; inside. This means that Items A1 and A2 are connected. Items A1 and A2 do not have to be counted.</p>
    <p>
        Similarly Items B1 and B2 are diamonds with the letter &quot;R&quot; inside. This means that Items B1 and B2 are connected. Items B1 and B2 do not have to be counted.</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_4.png" /></p>
    <h4>
        Stream3</h4>
    <h4>
        &nbsp;</h4>
    <h2>
        Gas line Detection</h2>
    <p>
        The figures below are of a vertical tank.</p>
    <p>
        In the figure below there are two level gauges attached to the tank. these are marked LG and are circled in red. The first one is connected to 2 locations and the line is highlighted in red. the second one only connects at one location and the line is highlighted in purple.</p>
    <p>
        When the code sees a vertical tank and either the first gauge (with 2 connections as marked in red ) or a combination of 2 or more with either type of connection then the code will mark all the streams above the halfway mark as&quot; gas &quot; and will mark all the streams below the halfway mark as&quot; liquid &quot; referring to the figure it can be seen that:</p>
    <p>
        The halfway mark is a dashed orange line</p>
    <p>
        The gas lines are yellow</p>
    <p>
        the liquid lines are blue</p>
    <p>
        Note the halfway mark does not have to be 100% accurate just roughly halfway between the highest Level gauge and the lowest level gauge. if its a little higher than the exact halfway mark ( by say 30%) or a little lower ( by the same amount) then this is fine.</p>
    <p>
        Once the halfway mark is set then all the lines above the halfway mark should be gas and all the ones below should be liquid.</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_5.png" />Figure LG1</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_6.png" /></p>
    <h2>
        TRY it with your own P&amp;ID for FREE.</h2>
    <h3>
        Application Limitations:</h3>
    <p>
        The designated level of accuracy is not guaranteed for the following:</p>
    <p>
        - Fuzzy images, incomplete components, overlapping components (and text/lines). These are issues that also introduce errors for manual parts counts.</p>
    <p>
        - If the components layout results in a particular component being categorised as under more than one classification then we cannot include this particular item in the evaluation of accuracy. See example below:</p>
    <p>
        <img src="uploads/userfiles/images/parst_count_7.png" /></p>
</div>
