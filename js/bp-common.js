$(function(){

	"use strict";

	var activeElem = null;

	$('.nav .dropdown-toggle').hover(
	   function(){
	   		if(!$(this).parent().hasClass('active')){
	   			$(this).parent().addClass('active');
	   		}	
			return false;
	   },
	   function(){
	   		if($(this).parent().hasClass('active')){
	   			$(this).parent().removeClass('active');
	   		}
			return false;
	  });

	  $('.nav .dropdown-menu').hover(
	  	   function(){
	  	   		if(!$('.nav .dropdown-toggle').parent().hasClass('active')){
	  	   			$('.nav .dropdown-toggle').parent().addClass('active');
	  	   		}	
	  			return false;
	  	   },
	  	   function(){
	  	   		if($('.nav .dropdown-toggle').parent().hasClass('active')){
	  	   			$('.nav .dropdown-toggle').parent().removeClass('active');
	  	   		}
	  			return false;
	  	  }
	  );	

	    $('.nav > li').hover(
		  	function(){
		  		activeElem = $(this).hasClass('active') ? $(this) : null;  	
		  		if(!activeElem) {
			  		$(this).addClass('active');	
		  		}
		  	},
		  	function(){
  		  		if(!activeElem) {
			  		$(this).removeClass('active');	
  		  		}
		  	}
	  	);

});