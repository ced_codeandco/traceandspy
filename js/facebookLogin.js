/**
 * Created by USER on 5/10/2015.
 */

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1631314640451199',
        xfbml      : true,
        version    : 'v2.5'
    });

    FB.getLoginStatus(function(response){
        runFbInitCriticalCode();
    });
};

function runFbInitCriticalCode()
{
    $(document).ready(function () {
        $('#facebookLoginLink').click(function () {
            myFacebookLogin();
        }).show();
    })
}


function myFacebookLogin() {
    //$('#facebookLoginButton').hide();
    //$('#facebookLoader').show();
    $('#fbloader-wrap').show();

    FB.login(function(response){
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            //testAPI(response);
            getDetails();
        } else {
            $('#facebookLoader').find('.msg').html('Authentication failed').addClass('error');
            $('#facebookLoader').find('img').hide();
            $('#fbloader-wrap').hide();
        }

    }, {scope: 'public_profile,email'});


}

function getDetails(response) {
    $('#facebookLoader').find('.msg').html('Retrieving user details...');
    FB.api('/me?fields=id,first_name,last_name,email,age_range,hometown,gender', function(response) {
        parseFbUserDetails(response);
    });
}


function parseFbUserDetails(fbResponse) {
    $('.facebook-auth, .oauth-wrap.google').hide();
    var formData = {
        'socialMedia': 'facebook',
        'socialMediaId' : fbResponse.id,
        'email' : fbResponse.email,
        'first_name' : fbResponse.first_name,
        'last_name' : fbResponse.last_name,
        /*'gender' : capitalizeFirstLetter(fbResponse.gender),*/
    };
    //fillRegistrationForm(formData);
    //createSocialAccount(formData);
    createUserAccountAndLogin(formData);
}

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


/*
$('#facebookLoginLink').click(function(){
    $('#facebook').find('.loadingFb').removeClass('error')
    $('#facebook').hide();
    $('#loadingIconContainer').show();
    FB.login(function(response) {
        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            $('#loadingIconContainer').find('.loadingFb').html('Fetching Information');
            FB.api('/me', function(response) {
                //console.log('Good to see you, ' + response.name + '.');
                $('#loadingIconContainer').find('.loadingFb').html('Creating Account');
                console.log(response); return;
                createUserAccountAndLogin(response);
            });
        } else {
            $('#facebook').find('div.connect').html('Failed to login').addClass('error')
            $('#facebook').show();
            $('#loadingIconContainer').hide();
            //console.log('User cancelled login or did not fully authorize.');
        }
    },{scope: 'public_profile,email',return_scopes: true});
})*/
function createUserAccountAndLogin(formData)
{
    var rootUrlLink = $('#rootUrlLink').val();
    var loginRedirect = $('#login-redirect').val();
    //showWait($(obj));
    jQuery.ajax({
        type: "POST",
        url: rootUrlLink+'facebookLogin',
        dataType: 'json',
        data: formData,
        cache: false,
        success: function(data) {
            //hideWait($(obj));
            var status = data.status;
            var msg = data.message;
            if(status*1 == 1)
            {
                window.location = loginRedirect;
            }
            else
            {
                alert(msg);
            }
            return data;
        }
    });
}