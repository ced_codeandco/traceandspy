$(document).ready(function(){
    $('.home-product-nav-control,#footer-contact-us').click(function(){
        var url = $(this).attr('data-url')
        if (typeof url != 'undefined' && url != '') {
            window.location = (url);
        }
    })
    $('.home_item_list_wrap').each(function(){
        var item_wrap = $(this);
        $(this).find('h2').click(function(){
            var url = $(item_wrap).find('.home-product-nav-control').attr('data-url');
            window.location = (url);
        })
    })
    $('.product-listing').each(function(){
        var item_wrap = $(this);
        $(this).find('h2,img').click(function(){
            var url = $(item_wrap).attr('data-url');
            window.location = (url);
        })
    })
})