/**
 * Created by USER on 11/19/2015.
 */

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {

    $('#linkedInBtn').click(function(){
        //alert("Innnn");
        /*IN.User.authorize(function(data){
         alert("Authorize");
         });*/
        IN.UI.Authorize().place();
        IN.Event.on(IN, "auth", function () { onLogin(); });
        //IN.Event.on(IN, "logout", function () { onLogout(); });
    })
}

function onLogin() {
    IN.API.Profile("me").fields("id","first-name", "last-name", "email-address").result(displayResult);
}

function displayResult(profiles) {
    member = profiles.values[0];
    //alert(member.id + " Hello " +  member.firstName + " " + member.lastName);
    var formData = {
        'socialMedia': 'linkedIn',
        'socialMediaId' : member.id,
        'email' : member.emailAddress,
        'first_name' : member.firstName,
        'last_name' : member.lastName,
        /*'gender' : capitalizeFirstLetter(fbResponse.gender),*/
    };
    LinkedCreateUserAccountAndLogin(formData);
}

function LinkedCreateUserAccountAndLogin(formData) {
    var rootUrlLink = $('#rootUrlLink').val();
    var loginRedirect = $('#login-redirect').val();
    jQuery.ajax({
        type: "POST",
        url: rootUrlLink+'facebookLogin',
        dataType: 'json',
        data: formData,
        cache: false,
        success: function(data) {
            //hideWait($(obj));
            var status = data.status;
            var msg = data.message;
            if(status*1 == 1)
            {
                //Post to linkedIn
                if (data.first_login == 1) {
                    //shareContent();
                    window.location = rootUrlLink+'select_package/'+data.userId;
                } else {
                    window.location = loginRedirect;
                }
            }
            else
            {
                alert(msg);
            }
            return data;
        }
    });
}

function shareContent() {
    // Build the JSON payload containing the content to be shared
    var payload = {
        "comment": "Test: "+$('#linked-in-post').val(),
        "visibility": {
            "code": "anyone"
        }
    };

    IN.API.Raw("/people/~/shares?format=json")
        .method("POST")
        .body(JSON.stringify(payload))
        .result(onSuccess)
        .error(onError);
}

function onSuccess(){ alert("Posted successfully"); }
function onError(){ alert("Post Failed"); }