<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('MAX_BANNER_IMAGE_SIZE',200);
define('MAX_PRODUCT_IMAGE_SIZE',5000);
define('IMAGE_ALLOWED_TYPES','gif|jpg|png|jpeg');

define('MAX_FILE_SIZE', 5242880); // 5 MB
define('ALLOWED_FILE_TYPES','pdf');

define('RECORD_PER_PAGE',3);
define('TIPS_PER_PAGE ',10);

define('PROJECT_NAME', 'pid');
define('SITE_LINK_TEXT', 'pandid.ae');
define('SITE_NAME', 'Trace & Spy');
define('DEFAULT_EMAIL', 'info@pidpartscount.com');

define('ROOT_PATH', ROOT_DIRECTORY);
if (!empty($_SERVER['HTTP_HOST'])) {
    define('ROOT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/');
    define('ROOT_URL_BASE', 'http://' . $_SERVER['HTTP_HOST'] . '/');
} else {
    define('ROOT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/');
    define('ROOT_URL_BASE', 'http://' . $_SERVER['HTTP_HOST'] . '/');
}
define('LOG_ENTRY', TRUE);
//define('ROOT_URL_BASE', 'http://'.$_SERVER['SERVER_NAME'].'/');

define('CSS_PATH', ROOT_URL_BASE.'css/' );
define('JS_PATH', ROOT_URL_BASE.'js/' );

define('ADMIN_ROOT_URL', ROOT_URL.'administrator/' );
define('ADMIN_ROOT_PATH', ROOT_URL_BASE.'administrator/' );
define('ADMIN_CSS_PATH', CSS_PATH.'admin/' );
define('ADMIN_JS_PATH', JS_PATH.'admin/' );

define('MEMBER_ROOT_URL', ROOT_URL.'member/' );
define('MEMBER_ROOT_PATH', ROOT_PATH.'member/' );
define('MEMBER_CSS_PATH', CSS_PATH.'member/' );
define('MEMBER_JS_PATH', JS_PATH.'member/' );

define('DIR_UPLOAD_BANNER', ROOT_PATH.'uploads/banners/');
define('DIR_UPLOAD_BANNER_SHOW', ROOT_URL_BASE.'uploads/banners/');
define('DIR_UPLOAD_CLASSIFIED', ROOT_PATH.'uploads/classifieds/');
define('DIR_UPLOAD_CLASSIFIED_SHOW', ROOT_URL_BASE.'uploads/classifieds/');
define('DIR_UPLOAD_ADVERTIZE', ROOT_PATH.'uploads/advertize/');
define('DIR_UPLOAD_ADVERTIZE_SHOW', ROOT_URL_BASE.'uploads/advertize/');
define('DIR_UPLOAD_BLOG', ROOT_PATH.'uploads/blog/');
define('DIR_UPLOAD_BLOG_SHOW', ROOT_URL_BASE.'uploads/blog/');

//define('DIR_UPLOAD_FILE', ROOT_PATH.'uploads/userfiles/uploaded_files/');

define('DIR_UPLOAD_FILE', 'C:\Users\\');
define('DIR_UPLOAD_FILE_SHOW', ROOT_URL_BASE.'uploads/userfiles/uploaded_files/');

define('TEMP_DIR_UPLOAD_FILE', ROOT_PATH.'uploads/tmp/');
define('TEMP_DIR_UPLOAD_FILE_SHOW', ROOT_URL_BASE.'uploads/tmp/');

define('DIR_UPLOAD_FILE_CONVERTED', ROOT_PATH.'uploads/userfiles/converted_files/');
define('DIR_UPLOAD_FILE_CONVERTED_SHOW', ROOT_URL_BASE.'uploads/userfiles/converted_files/');

/*
|--------------------------------------------------------------------------
| File path and Status
|--------------------------------------------------------------------------
|
| These status used on file conversion
|
*/
define('FILE_PATH_CONVERSION_FOLDER',	ROOT_PATH.'config/folder.txt');
define('FILE_PATH_CONVERSION_STATUS',	ROOT_PATH.'config/status.txt');
define('FILE_PATH_CONVERSION_EMAIL',	ROOT_PATH.'config/email.txt');

define('FILE_STATUS_UPLOADED',	'1');
define('FILE_STATUS_CONVERTED', '2');


/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/*
|--------------------------------------------------------------------------
| Terms popup cms page link
|--------------------------------------------------------------------------
|
|
*/
define('CMS_HOME_PAGE_ID', 1);
define('CMS_WHT_WE_DO_PAGE_ID', 2);
define('CMS_DISC_RELN_CHEAT_PAGE_ID', 7);
define('CMS_PROTECT_CHILDREN_PAGE_ID', 8);
define('CMS_MONITOR_WORKER_PAGE_ID', 9);
define('CMS_PRODUCT_PAGE_ID', 3);
define('CMS_CONTACT_US_PAGE_ID', 4);
define('CMS_LOGIN_PAGE_ID', 25);
define('CMS_FORGET_PWD_PAGE_ID', 26);
define('CMS_RESET_PWD_PAGE_ID', 27);

define('CMS_TERMS_POPUP_PAGE_ID', 5);
define('CMS_PRIVACY_POPUP_PAGE_ID', 19);
define('CMS_REFUND_POPUP_PAGE_ID', 21);
/*
|--------------------------------------------------------------------------
| custom Page urls
|--------------------------------------------------------------------------
|
|
*/
define('CMS_PRODUCT_PAGE_URL', 'products');
/*
|--------------------------------------------------------------------------
| Category ids
|--------------------------------------------------------------------------
|
| These values are used identifying the categories
|
*/
define('CATEG_REAL_EST_ID', 1);
define('CATEG_VEHICLES_ID', 2);
define('CATEG_BUY_SELL_ID', 3);
define('CATEG_JOB_ID', 4);
define('CATEG_SERVICE_ID', 6);
define('CATEG_COMMUNITY_ID', 7);
/*
|--------------------------------------------------------------------------
| SMTP configurations
|--------------------------------------------------------------------------
|
| These values are used for esnding emails
|
*/
define('DEFAULT_FROM_EMAIL', 'info@pidpartscount.com');
define('DEFAULT_FROM_NAME', 'P&ID');
define('SMTP_PASSWORD', 'Q56pek5%u6');
/*
|--------------------------------------------------------------------------
| Language configuration
|--------------------------------------------------------------------------
|
| Default language
|
*/
define('DEFAULT_LANGUAGE', 'EN');
define('DEFAULT_LANGUAGE_ID', 1);

//define('DEFAULT_LANGUAGE', 'AF');

/* End of file constants.php */
/* Location: ./application/config/constants.php */