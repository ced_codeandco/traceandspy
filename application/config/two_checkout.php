<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| 2Checkout parameters
|--------------------------------------------------------------------------
|
|
*/
$config['two_co_account_number'] = '901300512';
$config['two_co_account_mode'] = 'demo';
$config['two_co_post_url_live'] = 'https://www.2checkout.com/checkout/purchase';
$config['two_co_post_url_sandbox'] = 'https://sandbox.2checkout.com/checkout/purchase';
$config['two_co_secret_word'] = 'P&IDTOOL';
