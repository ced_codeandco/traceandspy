<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Products extends Public_controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $headerData;
    public $contentData;
    public $footerData;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('products_model', 'member_model'));
    }
    public function index()
    {
        $this->load->library('session');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }

        $start_with = !empty($_GET['per_page']) ? $_GET['per_page'] : 0;
        $this->contentData['cmsData'] = $this->cms_model->getDetails(CMS_PRODUCT_PAGE_ID);
        $result = $this->products_model->getProductsForList('*', "is_active = '1' AND is_deleted = '0'", 'ORDER BY product_order ASC', "LIMIT $start_with, ".RECORD_PER_PAGE, TRUE);
        $this->contentData['products_list'] = (!empty($result['result']) ? $result['result'] : array());
        $total_row_count = !empty($result['total_row_count']) ? $result['total_row_count'] : 0;

        $this->contentData['paginator'] = $this->products_model->build_paginator($total_row_count, RECORD_PER_PAGE);

        $this->headerData['seo'] = $this->cms_model->populateSeoTags($this->contentData['cmsData']);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('products', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function details($page_slug = '')
    {
        $this->load->library('session');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }
        if ($productData = $this->products_model->getPageDetails($page_slug)) {
            $this->contentData['productData'] = $productData;
            $this->contentData['show404'] = false;
            $this->footerData['footer_text'] = !empty($productData->footer_text) ? $productData->footer_text : '';
            if (!empty($productData)) {
                $this->headerData['seo'] = $this->cms_model->populateSeoTags($productData);
            }
        } else {
            show_404('Product not found', true);
            exit;
            $this->contentData['show404'] = true;
        }
        $this->contentData['cmsData'] = $this->cms_model->getDetails(CMS_PRODUCT_PAGE_ID);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('product_details', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function buy($product_id) {
        $this->load->library('session');

        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }
        if ($productData = $this->products_model->getDetails($product_id)) {
            $this->contentData['productData'] = $productData;
            $this->contentData['show404'] = false;
            $this->footerData['footer_text'] = !empty($productData->footer_text) ? $productData->footer_text : '';
            if (!empty($productData)) {
                $this->headerData['seo'] = $this->cms_model->populateSeoTags($productData);
            }
        } else {
            show_404('Product not found', true);
            exit;
            $this->contentData['show404'] = true;
        }
        if ($this->member_model->validate_user_info()) {
            $dateOfBirthField = strtotime($this->input->post('date_of_birth'));
            $dob = date('Y-m-d', $dateOfBirthField);
            $_POST['date_of_birth'] = $dob;
            //Register
            $emailVerificationToken = trim(strtolower(base64_encode(uniqid('', true) . "-" . date("YmdHis"))));
            $user_id = $this->member_model->addDetails($emailVerificationToken);
            $this->member_model->sendVerificationEmailOnRegistration($emailVerificationToken, $user_id);

            redirect(ROOT_URL.'confirm_payment/'.$user_id.'/'.$product_id);
            exit;
        }
        $this->contentData['product_id'] = $product_id;
        $this->contentData['cmsData'] = $this->cms_model->getDetails(CMS_PRODUCT_PAGE_ID);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('user_info', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
        /*$packageDetails = $this->products_model->getDetails($product_id);
        $data['package_cart'] = array(
            'package_id' => $this->input->post('package_id'),
            'amount' => $packageDetails->price,
            'package_file_count' => $packageDetails->files_count,
        );
        $this->session->set_userdata($data);

        $this->config->load('two_checkout');
        $this->contentData['two_co_account_number'] = config_item('two_co_account_number');
        $this->contentData['two_co_account_mode'] = config_item('two_co_account_mode');
        if ($this->contentData['two_co_account_mode'] == 'demo') {
            $this->contentData['two_co_post_url'] = config_item('two_co_post_url_sandbox');
        } else {
            $this->contentData['two_co_post_url'] = config_item('two_co_post_url_live');
        }
        $this->contentData['cmsData'] = $this->cms_model->getDetails(CMS_PRODUCT_PAGE_ID);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('two_checkout_form', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);*/
    }

    public function confirm_payment($user_id, $product_id) {
        if ($productData = $this->products_model->getDetails($product_id)) {
            $this->config->load('two_checkout');
            $this->contentData['two_co_account_number'] = config_item('two_co_account_number');
            $this->contentData['two_co_account_mode'] = config_item('two_co_account_mode');
            if ($this->contentData['two_co_account_mode'] == 'demo') {
                $this->contentData['two_co_post_url'] = config_item('two_co_post_url_sandbox');
            } else {
                $this->contentData['two_co_post_url'] = config_item('two_co_post_url_live');
            }

            $this->contentData['productData'] = $productData;
            $this->contentData['show404'] = false;
            $this->footerData['footer_text'] = !empty($productData->footer_text) ? $productData->footer_text : '';
            if (!empty($productData)) {
                $this->headerData['seo'] = $this->cms_model->populateSeoTags($productData);
            }
        } else {
            show_404('Product not found', true);
            exit;
            $this->contentData['show404'] = true;
        }

        $this->contentData['member_id'] = $user_id;
        $this->contentData['cmsData'] = $this->cms_model->getDetails(CMS_PRODUCT_PAGE_ID);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('buy_now', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function payment_status() {
        {
            $credit_card_processed = $this->input->post('credit_card_processed');
            $status = 'failed';
            if(!empty($credit_card_processed) && $credit_card_processed =='Y') {
                //$this->load->model('package_model');
                $this->config->load('two_checkout');
                $hashSecretWord = config_item('two_co_secret_word');
                $two_co_account_mode = config_item('two_co_account_mode');
                $hashSid = $this->input->post('sid');//2Checkout account number
                $hashTotal = $this->input->post('total');//Sale total to validate against
                $hashOrder = (!empty($two_co_account_mode) && $two_co_account_mode == 'demo') ? 1 : $this->input->post('order_number'); //2Checkout Order Number


                $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));

                if ($StringToHash != $this->input->post('key')) {
                    $result = 'Fail - Hash Mismatch';
                    $this->session->set_flashdata('flash_error', 'Sorry! We could not authenticate your payment. Please contact your bank for details');
                    $status = 'failed';
                } else {
                    $result = 'Success - Hash Matched';

                    $custom_userid = $this->input->post('custom_userid');
                    $billing_email = $this->input->post('email');
                    $order_number = $this->input->post('order_number');
                    $invoice_id = $this->input->post('invoice_id');
                    $total = $this->input->post('total');
                    $ip_country = $this->input->post('ip_country');
                    $product_id = $this->input->post('custom_product_id');

                    $productData = $this->products_model->getDetails($product_id);
                    //$packageDetails = $this->package_model->getDetails($package_id);
                    //print_r($packageDetails);
                    $data = array(
                        'member_id' => $custom_userid,
                        'billing_email' => $billing_email,
                        'order_number' => $order_number,
                        'invoice_id' => $invoice_id,
                        'price' => $total,
                        'ip_country' => $ip_country,
                        'product_id' => $product_id,
                        'current_package_start_date' => date('Y-m-d H:i:s'),
                        //'current_package_end_date' => $this->package_model->calculate_package_end_date($packageDetails->duration_value),
                        'status' => '1',
                        'created_by' => $custom_userid,
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_ip' => $this->input->ip_address(),
                        'is_active' => '1'
                    );
                    $payment_id = $this->products_model->add_member_purchase($data, $custom_userid);

                    //$this->add_member_purchase->sendPackageEmail($data, $productData);
                    //$this->session->set_flashdata('flash_success', 'Payment received successfully');
                    $status = 'success';
                    //die("here - 1111");
                }
            } else {
                if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                    $this->session->set_flashdata('flash_error', 'Sorry! Payment not received.');
                } else {
                    $this->session->set_flashdata('flash_error', 'Sorry! Payment not received. Please check your inbox for account activation email');
                }
                $status = 'failed';
            }

            if ($status == 'failed') {
                $this->session->set_flashdata('flash_success', 'Registration completed successfully. Please check you inbox for account activation email.');
            } else if ($status == 'success') {
                if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                    $this->session->set_flashdata('flash_success', 'Payment received successfully');
                } else {
                    $this->session->set_flashdata('flash_success', 'Registration completed successfully. Please check you inbox for account activation email.');
                }
            }
            redirect(ROOT_URL . "generate_credentials/$custom_userid/$product_id/$payment_id");
            exit;
            //
            if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                redirect(ROOT_URL . 'my_products');
                exit;
            } else {
                redirect(ROOT_URL . 'login');
                exit;
            }
        }
    }

    public function generate_credentials($user_id, $product_id, $payment_id) {
        $member_details = $this->member_model->getDetails($user_id);
        $product_details = $this->products_model->getDetails($product_id);
        $payment_details = $this->products_model->getPaymentDetails($payment_id);
        if (empty($member_details) || empty($product_details) || empty($payment_details)) {
            die("here");
        } else {
print_r($member_details->email);
            $this->config->load('mag_pie');
            $portal_uri = config_item('portal');
            $resellerId = config_item('resellerId');
            $item_code = config_item('item_code');


            $api_url = $portal_uri . "/api/reseller/newLicense";
            $params = array(
                'customerEmail' => rawurlencode(utf8($member_details->email.'-aa_b b.cc~dd')),
                'resellerId' => rawurlencode($resellerId),
                'retailItem' => rawurlencode($item_code),
            );

            natsort($params);
            natsort($params);
            echo "<pre>";
            print_r($params);
        }
    }

    public function valid_captcha($str) {
        session_start();
        if ($_SESSION['captcha'] != $str) {
            $this->form_validation->set_message('valid_captcha', 'Invalid captcha');
            return false;
        }

        return true;
    }

    public function news_list() {
        $this->load->model('news_model');
        $this->load->library('pagination');

        $count_result = $this->news_model->getCount("is_active='1'");

        $cur_page = $this->input->get('per_page');
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['base_url'] = ROOT_URL.'/news_list?';
        $config['total_rows'] = $count_result;
        $config['per_page'] = 10;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<span >&lt;&lt;</span>';;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<span >&lt;</span>';
        $config['prev_tag_open'] = '<li class="prev_li" >';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<span >&gt;</span>';
        $config['next_tag_open'] = '<li class="next_li">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active_li"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';

        $config['last_link'] = '<span >&gt;&gt;</span>';;
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->contentData['pagination'] = $this->pagination->create_links();

        $limit_string = empty($cur_page) ? " LIMIT $config[per_page]" : " LIMIT $cur_page, $config[per_page]";
        $this->contentData['news_list'] = $this->news_model->getAllRecords('id,title,url_slug,small_description',"is_active='1' AND is_deleted = '0'", "ORDER BY sort_order ASC", $limit_string);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('news_list', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function news($page_slug)
    {
        $this->load->model('news_model');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'News';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }
        if ($cmsData = $this->news_model->getPageDetails($page_slug)) {
//            print_r($cmsData);
            $this->contentData['cmsData'] = $cmsData;
            $this->contentData['show404'] = false;
        } else {
            $this->contentData['show404'] = true;
        }


        if (!empty($wrapperType) && $wrapperType =='ajax') {
            //$this->load->view('templates/header_ajax', $this->headerData);
            $this->load->view('ajax-cms', $this->contentData);
            //$this->load->view('templates/footer_ajax', $this->footerData);
        } else {
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('news', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }

        /*$this->load->view('templates/header', $this->headerData);
        $this->load->view('cms', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);*/
    }

    function sendContactEmail()
    {
        $admin_subject = "New Inquiry on ".SITE_NAME;
        $user_subject = "Thank you for Inquiry on ".SITE_NAME;

        $admin_header = "Dear Administrator,<br /><br />".$_POST['name']." has submitted contact form with below details on ".SITE_NAME.". <br /><br />";
        $user_header = "Dear ".$_POST['name'].",<br /><br /> Thank you for Inquiry on ".SITE_NAME." with below details. <br /><br />";

        $emailMessage  = "<strong>Detail :</strong> <br /><br />";
        $emailMessage .= "Name   		  : ".$_POST['name']." <br /><br />";

        if(isset($_POST['email']) && $_POST['email'] != '')
            $emailMessage .= "E-mail  	  : ".$_POST['email']." <br /><br />";

        if(isset($_POST['telephone']) && $_POST['telephone'] != '')
            $emailMessage .= "Telephone  	  : ".$_POST['telephone']." <br /><br />";

        if(isset($_POST['comments']) && $_POST['comments'] != '')
            $emailMessage .= "Comments   	  : ".$_POST['comments']." <br /><br />";

        $emailMessage .= "Thanks <br />".SITE_NAME." Team";


        /*$headers = 'From: '.DEFAULT_EMAIL . "\r\n" .
            'Reply-To: '.DEFAULT_EMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";*/
        $this->load->library('emailclass');
        $content = $this->emailclass->emailHeader();
        $content .= $emailMessage;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$content);
        $email = $this->emailclass->send_mail($_POST['email'], $user_subject, $user_header.$content);


        /*if (mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$emailMessage, $headers)){
            mail($_POST['email'], $user_subject, $user_header.$emailMessage, $headers);
        }*/
        /*if(mail(DEFAULT_EMAIL,SITE_NAME,$_POST['email'],$_POST['name'],$user_subject,$user_header.$emailMessage)){

            sendMail(DEFAULT_EMAIL,SITE_NAME,SUPPORT_EMAIL, SITE_NAME, $admin_subject,$admin_header.$emailMessage);
            return true;
        }*/
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */