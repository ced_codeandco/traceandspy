<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Cron_script extends Public_controller
{
    public $headerData;
    public $contentData;
    public $footerData;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('email');
    }

    public function index() {
        //echo "Hello {}".PHP_EOL;

        //Read status files
        $statusLogs = $this->classified_model->readLogFile(FILE_PATH_CONVERSION_STATUS);
        //print_r($statusLogs);
        //echo "\n\n\n";
        $counter =1;
        while (in_array(FILE_STATUS_CONVERTED, $statusLogs)) {
            //echo "\n\nInnnnnn\n\n SI: - $counter";
            $key = array_search(FILE_STATUS_CONVERTED, $statusLogs);
            //echo "\t\t".$key;
            $emailLog = $this->classified_model->readLogFile(FILE_PATH_CONVERSION_EMAIL);
            $userEmail = !empty($emailLog[$key]) ? $emailLog[$key] : '';
            $userDetails = $this->member_model->checkEmailExist($userEmail);
            if (!empty($userDetails[0])) {
                $userDetails = $userDetails[0];
            }
            //print_r($userDetails); die();
            if (!empty($userEmail) && valid_email($userEmail) && $userDetails != false) {
                //Update DB - conversion status
                //$data = array('file_status' => FILE_STATUS_CONVERTED);
                $this->classified_model->updateFileConversionStatus($userDetails->id, FILE_STATUS_UPLOADED, FILE_STATUS_CONVERTED);
                //Send email
                $this->sendConvertedNotification($userDetails);

                //Update folder file
                $folderLogs = $this->classified_model->readLogFile(FILE_PATH_CONVERSION_FOLDER);
                if (isset($folderLogs[$key])) unset($folderLogs[$key]);
                $this->classified_model->updateLogsFromArray(FILE_PATH_CONVERSION_FOLDER, $folderLogs);

                //Update email file
                $emailLog = $this->classified_model->readLogFile(FILE_PATH_CONVERSION_EMAIL);
                if (isset($emailLog[$key])) unset($emailLog[$key]);
                $this->classified_model->updateLogsFromArray(FILE_PATH_CONVERSION_EMAIL, $emailLog);

                //Update status file
                $statusLogs = $this->classified_model->readLogFile(FILE_PATH_CONVERSION_STATUS);
                if (isset($statusLogs[$key])) unset($statusLogs[$key]);
                $this->classified_model->updateLogsFromArray(FILE_PATH_CONVERSION_STATUS, $statusLogs);
            }
            $counter++;
        }
        //print_r($statusLogs);

        //Read database
        $fileList = $this->classified_model->getAllRecords('*'," is_active = '1' AND file_status = '1' ",'ORDER BY  created_date_time ASC');
        foreach ($fileList as $file) {
            $memberId = $file->created_by;
            if (!empty($memberId)) {

                $userDetails = $this->member_model->getDetails($memberId);
                //echo "\t".$userDetails->email;
                if (!empty($userDetails) && !empty($userDetails->email)) {
                    $this->classified_model->update_log_file($userDetails->email, FILE_STATUS_UPLOADED);
                }
            }
        }

        //$this->update_log_file($userEmail, FILE_STATUS_UPLOADED);

        //Update status files
        /*$condition = " is_active ='1' AND is_deleted = '0' AND file_status = '1' ";
        $records = $this->classified_model->getAllRecords('*', $condition);*/
        exit;
    }

    private function sendConvertedNotification($userDetails)
    {
        $this->load->library('emailclass');
        $message = "<p>Dear " . $userDetails->first_name . ",</p><br />";

        $message .= "<p><strong>Congratulations!!</strong> Your files are converted successfully on " . SITE_NAME . " </p>";
        $message .= "<p>Please use the below link to download your files</p>";
        $message .= '<p><a target="_blank" href="' . MEMBER_ROOT_URL . 'my_files">' . MEMBER_ROOT_URL . "my_files</a></p>";
        $subject = 'File converted successfully  ' . SITE_NAME;
        $content = '';
        $content .= $this->emailclass->emailHeader();
        $content .= $message;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail($this->input->post('email'), $subject, $content);
    }
}