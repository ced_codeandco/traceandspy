<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Products extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('products_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive($id=0){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$productsId =  $this->uri->segment(4);
			if($productsId == ''){
				redirect(ADMIN_ROOT_URL.'products');
			}else{
				$this->products_model->changeStatus(0,$productsId);
				$this->session->set_flashdata('flash_success', 'Product Status changed successfully');
				redirect(ADMIN_ROOT_URL.'products');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$productsId =  $this->uri->segment(4);
			if($productsId == ''){
				redirect(ADMIN_ROOT_URL.'products');
			}else{
				$this->products_model->changeStatus(1,$productsId);
				$this->session->set_flashdata('flash_success', 'Product Status changed successfully');
				redirect(ADMIN_ROOT_URL.'products');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$productsId =  $this->uri->segment(4);
			
				$this->products_model->deleteRecord($productsId);
				$this->session->set_flashdata('flash_success', 'Product deleted successfully');
				redirect(ADMIN_ROOT_URL.'products');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$productsId =  $this->uri->segment(4);
			$action = 'Add';
			if($productsId == ''){
				$action = 'Add';
				$this->contentData['productDetails'] = array();
			}else{
				$action = 'Edit';
				$productDetails = $this->products_model->getDetails($productsId);
				$this->contentData['productDetails'] = $productDetails;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor);

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['product_banner_image']) && $_FILES['product_banner_image']['name']!=''){
					$this->form_validation->set_rules('product_banner_image', 'Image', 'trim|callback_upload_image');
				}else{
					$_POST['product_banner_image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';
				}

					
				if ($this->form_validation->run() == TRUE)
				{
                    $_POST['bullet_description'] = serialize($_POST['bullet_description']);
					if($this->input->post('action') == 'Add') {					
						$_POST['product_slug'] = $this->products_model->generateCleanSlug($this->input->post('title'));
						$_POST['description'] = addslashes($_POST['description']);
						$insertedId = $this->products_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Product Details Added successfully');
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'products/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'products');
						}
					}else{
						
						$updateStatus = $this->products_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Product Details Updated successfully');
							
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'products/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'products');
						}
					}
				}else{
					$_SESSION = $_POST;	
				}
				
			}
			$this->contentData['parentPageList'] = $this->products_model->getAllRecords('id, title' ,' parent_id=0',' ORDER BY product_order ASC');
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Product | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_products', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
		
		$updateStatus = $this->products_model->changeOrder($_GET['id'],$_GET['product_order'],$_GET['position']);
		$this->session->set_flashdata('flash_success', 'Product Order Updated successfully');
		if(isset($_GET['parent']) && $_POST['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'products/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'products');
								
	}
	function upload_image(){
        $config['upload_path'] = DIR_UPLOAD_BANNER;
        $config['allowed_types'] = IMAGE_ALLOWED_TYPES;
        $config['max_size']	= MAX_PRODUCT_IMAGE_SIZE;

        $file_parts = pathinfo($_FILES['product_banner_image']['name']);
        $file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $file_parts['filename']);
        $config['file_name'] = $file_name.'.'.$file_parts['extension'];
        $counter = 0;
        while (file_exists($config['upload_path'].$config['file_name'])) {
            $counter++;
            $config['file_name'] = $file_name.'_'.$counter.'.'.$file_parts['extension'];
        }
		$_POST['product_banner_image'] = $config['file_name'];

        $this->load->library('upload');
        $this->upload->initialize($config);

		if ($this->upload->do_upload('product_banner_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function upload_image_alt_1(){
        $config['upload_path'] = DIR_UPLOAD_BANNER;
        $config['allowed_types'] = IMAGE_ALLOWED_TYPES;
        $config['max_size']	= MAX_BANNER_IMAGE_SIZE;

        $file_parts = pathinfo($_FILES['additional_image_1']['name']);
        $file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $file_parts['filename']);
        $config['file_name'] = $file_name.'.'.$file_parts['extension'];
        $counter = 0;
        while (file_exists($config['upload_path'].$config['file_name'])) {
            $counter++;
            $config['file_name'] = $file_name.'_'.$counter.'.'.$file_parts['extension'];
        }
		$_POST['additional_image_1'] = $config['file_name'];

		$this->load->library('upload');
        $this->upload->initialize($config);

		if ($this->upload->do_upload('additional_image_1'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file_1']) && $_POST['uploaded_file_1']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file_1'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file_1']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image_alt_1', $this->upload->display_errors());
			return FALSE;
		}

	}
	function upload_image_alt_2(){
        $config['upload_path'] = DIR_UPLOAD_BANNER;
        $config['allowed_types'] = IMAGE_ALLOWED_TYPES;
        $config['max_size']	= MAX_BANNER_IMAGE_SIZE;

        $file_parts = pathinfo($_FILES['additional_image_2']['name']);
        $file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $file_parts['filename']);
        $config['file_name'] = $file_name.'.'.$file_parts['extension'];
        $counter = 0;
        while (file_exists($config['upload_path'].$config['file_name'])) {
            $counter++;
            $config['file_name'] = $file_name.'_'.$counter.'.'.$file_parts['extension'];
        }
		$_POST['additional_image_2'] = $config['file_name'];

		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('additional_image_2'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file_2']) && $_POST['uploaded_file_2']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file_2'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file_2']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image_alt_2', $this->upload->display_errors());
			return FALSE;
		}

	}
	function upload_image_alt_3(){
        $config['upload_path'] = DIR_UPLOAD_BANNER;
        $config['allowed_types'] = IMAGE_ALLOWED_TYPES;
        $config['max_size']	= MAX_BANNER_IMAGE_SIZE;

        $file_parts = pathinfo($_FILES['additional_image_3']['name']);
        $file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $file_parts['filename']);
        $config['file_name'] = $file_name.'.'.$file_parts['extension'];
        $counter = 0;
        while (file_exists($config['upload_path'].$config['file_name'])) {
            $counter++;
            $config['file_name'] = $file_name.'_'.$counter.'.'.$file_parts['extension'];
        }
		$_POST['additional_image_3'] = $config['file_name'];

		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('additional_image_3'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file_3']) && $_POST['uploaded_file_3']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file_3'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file_3']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image_alt_3', $this->upload->display_errors());
			return FALSE;
		}

	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{

			$this->contentData['productsList'] = $this->products_model->getAllRecords('id, title, product_slug,parent_id,product_banner_image, on_header, on_footer, is_active,created_date_time,product_order' ,'parent_id = '.$parentId,' ORDER BY product_order ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Product List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/products_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */