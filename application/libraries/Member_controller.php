<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_controller extends CI_Controller {

    /**
     * Holder for session values
     */
    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;
    public $language;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->model('member_model');
        $this->load->library(array('form_validation', 'session'));
        $this->load->model(
            array('classified_model', 'cms_model', 'admin_model', 'inquiry_model', 'home_page_image_model', 'setting_model')
        );

        $this->headerData['isMemberLogin'] = $this->member_model->checkMemberLogin();
        $this->headerData['activeMemberDetails'] = $this->member_model->activeMemberDetails();

        $this->setting_model->defineAllSettingVariables();
        $this->footerData['current_slug'] = $this->headerData['current_slug'] = $this->get_current_slug();
        $this->headerData['top_pages'] = $this->cms_model->get_cms_navigation();
        /*$this->footerData['current_slug'] = $this->headerData['current_slug'] = $this->get_current_slug();
        $this->headerData['top_pages'] = $this->cms_model->getAllRecords('id, title, cms_slug, is_popup', " on_header ='1' ", 'ORDER BY cms_order ASC');
        $this->footerData['bottom_pages'] = $this->cms_model->getAllRecords('id, title, cms_slug, is_popup', " on_footer ='1' ", 'ORDER BY cms_order ASC');*/
        $this->footerData['refund_popup_content'] = $this->cms_model->getDetails(CMS_REFUND_POPUP_PAGE_ID);
        $this->footerData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
        $this->user_id = $this->session->userdata('id');
        $this->is_member = $this->session->userdata('is_member');

        if (!empty($this->user_id) && $this->is_member == true && $this->is_admin == false && $this->headerData['isMemberLogin'] != false) {
            $this->is_logged_in = true;
            $this->headerData['isLoggedIn'] = $this->is_logged_in;
        }

        if($this->user_id == null || $this->user_id == '' || $this->is_member != true || $this->is_logged_in != true) {
            $safe_methods = array('login', 'logout', 'forgot_password', 'reset_password', 'register', 'verify_email', 'facebookLogin');
            $current_method = $this->router->fetch_method();
            if (!in_array($current_method, $safe_methods)) {
                $this->session->unset_userdata('is_member');
                $this->session->unset_userdata('id');
                $curerntUrl = current_url();
                $backUrl = base64_encode(str_replace(ROOT_URL, '', $curerntUrl));
                redirect(ROOT_URL.'login'.(!empty($backUrl) ? '?back='.$backUrl : ''));
                exit;
            }
        }
    }
    public function get_current_slug() {
        $current_url = current_url();
        $current_slug = str_replace(ROOT_URL, '', $current_url);

        return $current_slug;
    }
}