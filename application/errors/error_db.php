<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/animations.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/global.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/nav.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/contact.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/dev.css">

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<nav role="navigation" class="navbar navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo ROOT_URL_BASE;?>" class="navbar-brand"><img src="<?php echo ROOT_URL_BASE;?>images/logo.png"></a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse nav-menu-min-height">
            <ul class="nav navbar-nav navbar-right">

            </ul>
        </div>
    </div>
</nav>


<header class="bp-banner contact-bnr">

    <div class="wrapper">

        <div class="bnr-content">

            <h1>Sorry! Something went wrong.</h1>
            <p>Please try again later.</p>

        </div>

    </div>

</header>


<section class="contact" id="contact">



    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5 min-height-box">
                <a href="<?php echo ROOT_URL;?>">Please click here to go to home page</a>
            </div>
        </div>
    </div>



</section>


<footer class="footer">

    <div class="wrapper clearfix">

        <small>&copy; 2015. Trace &amp; Spy, Ltd. <a href="#" class="anchor">Terms and Conditions</a></small>

        <ul class="social-links">
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
        </ul>

    </div>


</footer>


<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bootstrap.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>

<script type="text/javascript">

    $(function(){

        $('.contact-bnr')
            .parallax({
                imageSrc: '<?php echo ROOT_URL_BASE;?>images/bg/products-bnr-bg.jpg'
            });

    });

</script>


</body>
</html>