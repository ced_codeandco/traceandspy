<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/products.css">
<header class="bp-banner products-bnr parallax-top-bnr">

    <div class="wrapper">

        <div class="bnr-content">

            <h1><?php echo $cmsData->title;?></h1>
            <p><?php echo $cmsData->small_description;?></p>

        </div>

    </div>

</header>
<section class="products" id="products">
    <div class="container">
        <?php if (!empty($products_list) && is_array($products_list)) {
            $counter = 1;
            foreach ($products_list as $product) { ?>
                <div class="row product-listing" data-url="<?php echo ROOT_URL.'details/'.$product->product_slug;?>">
                    <?php if ($counter %2 == 0) {?>
                    <div class="col-md-6">
                        <?php if (!empty($product->product_banner_image) && file_exists(DIR_UPLOAD_BANNER.$product->product_banner_image)) {?>
                            <img class="img-responsive iphone-six" id="iphone-six" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$product->product_banner_image;?>"
                                 alt="<?php echo $product->title?>">
                        <?php }?>
                    </div>
                    <?php }?>
                    <div class="col-md-6">

                        <h2><?php echo $product->title?></h2>

                        <p><?php echo $product->small_description?></p>
                        <?php
                        $bullet_description = (!empty($product->bullet_description)) ? $product->bullet_description : '';
                        $bullet_description = @unserialize($bullet_description);

                        if (!empty($bullet_description) && is_array($bullet_description)) {?>

                        <ul class="product-keypoints">
                        <?php foreach($bullet_description as $bullet) {
                            if (!empty($bullet)) {
                                ?>
                                <li><a href="javascript:void(0);"><?php echo $bullet;?></a></li>
                            <?php }
                        }?>
                        </ul>

                        <?php
                        }?>

                    </div>
                    <?php if ($counter %2 != 0) {?>
                    <div class="col-md-6">
                        <?php if (!empty($product->product_banner_image) && file_exists(DIR_UPLOAD_BANNER.$product->product_banner_image)) {?>
                            <img class="img-responsive iphone-six" id="iphone-six" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$product->product_banner_image;?>"
                                 alt="<?php echo $product->title?>">
                        <?php }?>
                    </div>
                    <?php }?>

                </div><?php
                $counter++;
            }
        }?>
        <?php echo $paginator;?>
    </div>

</section>
<?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
<?php } ?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
    $(function(){
        if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
            $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
        }
        if ($('#parallax-image-banner').length > 0 && $('#parallax-image-banner').val() != '') {
            $('.discover-cheating-bnr').parallax({imageSrc: $('#parallax-image-banner').val()});
        }
    });
</script>