<?php if ($show404 != true) {
    //print_r($cmsData);
    if (!empty($cmsData->id) && $cmsData->id == CMS_CONTACT_US_PAGE_ID) {?>
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/contact.css">
        <header class="bp-banner contact-bnr parallax-top-bnr">
            <div class="wrapper">
                <div class="bnr-content">
                    <h1><?php echo $cmsData->title;?></h1>
                    <p><?php echo $cmsData->small_description;?></p>
                </div>
            </div>
        </header>
        <section class="contact" id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <?php echo validation_errors();?>
                        <?php
                        if(isset($errMsg) && $errMsg != ''){ ?>
                            <div class="alert alert-danger">
                                <?php echo $errMsg;?>
                            </div>
                            <?php unset($errMsg);
                        }
                        if(isset($succMsg) && $succMsg != ''){ ?>
                            <div class="alert alert-success">
                                <?php echo $succMsg;?>
                            </div>
                            <?php unset($succMsg);
                        }?>
                        <form action="" method="POST" class="contact-form">
                            <input type="text" placeholder="Full Name*" name="name" required="required">
                            <input type="email" placeholder="Email Address*" name="email" required="required">
                            <input type="text" placeholder="Telephone" name="telephone">
                            <textarea name="comments" placeholder="Message/Comments*" required="required"></textarea>
                            <img id="captcha" src="<?php echo ROOT_URL_BASE;?>assets/captcha/captcha.php" />
                            <a class="refresh" href="javascript:void(0)" onclick="
								document.getElementById('captcha').src='<?php echo ROOT_URL_BASE;?>assets/captcha/captcha.php?'+Math.random();
								document.getElementById('captcha-form').focus();" id="change-image"><img src="<?php echo ROOT_URL_BASE;?>images/refresh.png" alt=""></a>
                            <div class="clearfix"></div>
                            <input type="text" placeholder="Enter the word shown above" name="captcha" id="captcha-form" required="required">
                            <div class="clearfix"></div>
                            <button class="btn btn-or">Send Message</button>
                        </form>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <?php if (!empty($cmsData->additional_image_2) && file_exists(DIR_UPLOAD_BANNER.$cmsData->additional_image_2)) {?>
                        <a href="javascript:void(0)">
                        <img class="contact-map img-responsive ajax-popup-iframe" href="<?php echo $cmsData->map_info;?>" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->additional_image_2;?>" alt="map image">
                        </a>
                        <?php }?>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="address-wrpr clearfix">
										<span class="icon-contact icon-marker">
										</span>
                            <div class="address">
                                <h5><?php echo (defined('CONTACT_COMPANY_NAME')) ? CONTACT_COMPANY_NAME : '';?></h5>
                                <pre><?php echo (defined('CONTACT_ADDRESS')) ? CONTACT_ADDRESS : '';?></pre>

                            </div>
                        </div>
                        <div class="address-wrpr clearfix">
										<span class="icon-contact icon-env">
										</span>
                            <div class="address">
                                <?php echo (defined('CONTACT_EMAIL')) ? '<pre><a href="mailto:'.CONTACT_EMAIL.'">'.CONTACT_EMAIL.'</pre>' : '';?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!--<script src="<?php /*echo ROOT_URL_BASE;*/?>js/jquery.validate.min.js"></script>-->

        <?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
            <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
        <?php }
    }  else {?>
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
        <header class="bp-banner discover-bnr parallax-top-bnr">

            <div class="wrapper">

                <div class="bnr-content">

                    <h1><?php echo $cmsData->title;?></h1>
                    <p><?php echo $cmsData->small_description;?></p>

                </div>

            </div>
        </header>
            <?php
            $cms_wrapper = 'discover-cheating';
            if ($cmsData->id == CMS_DISC_RELN_CHEAT_PAGE_ID){
                $cms_wrapper = 'discover-cheating';
            } else if ($cmsData->id == CMS_PROTECT_CHILDREN_PAGE_ID){
                $cms_wrapper = 'protect-children';
            } else if ($cmsData->id == CMS_MONITOR_WORKER_PAGE_ID){
                $cms_wrapper = 'monitor-workers';
            }?>
            <section class="<?php echo $cms_wrapper;?>">
                <div class="container cms_wrap">
                    <?php echo $cmsData->description;?>
                </div>
            </section>
            <section class="highlight-bnr discover-cheating-bnr">

                <div class="wrapper">
                    <h2><?php echo !empty($cmsData->footer_text) ? $cmsData->footer_text : '';?></h2>
                    <button class="btn" id="footer-contact-us" data-url="<?php echo !empty($contact_cms->cms_slug) ? ROOT_URL.$contact_cms->cms_slug : '';?>">
                        <?php echo !empty($contact_cms->title) ? $contact_cms->title : ''; //var_dump($contact_cms);?>
                    </button>
                </div>

            </section>
        <?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
            <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
        <?php }?>
        <?php if (!empty($cmsData->additional_image_2) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->additional_image_2) && file_exists(DIR_UPLOAD_BANNER.$cmsData->additional_image_2)) {?>
            <input type="hidden" id="parallax-image-banner" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->additional_image_2;?>" />
        <?php }?>
    <?php }?>
<?php } else {?>

<?php } ?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
$(function(){
    if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
        $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
    }
    if ($('#parallax-image-banner').length > 0 && $('#parallax-image-banner').val() != '') {
        $('.discover-cheating-bnr').parallax({imageSrc: $('#parallax-image-banner').val()});
    }
});
</script>

<?php return;?>
<div class="row cms_page_wrap">
    <div class="col-lg-12">
        <h3 class="text-left"><?php echo (!empty($show404)) ? 'Page not found' :$cmsData->title;?></h3>
    </div>
    <div class="devider-25px"></div>
    <?php if (!empty($cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <div class="col-lg-4">
        <center><img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" class="img-responsive"></center>
        <p>&nbsp;</p>
    </div><!-- /.col-lg-4 -->
    <?php }?>

    <?php
    if (!empty($cmsData->id) && $cmsData->id == CMS_CONTACT_US_PAGE_ID) {?>

        <div class="col-lg-4 ">
            <?php echo stripslashes($cmsData->description); ?>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <?php echo validation_errors();?>
            <?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>
            <form method="post" action="">
                <div class="form-group">
                    <label for="exampleInputEmail1">Your Name*</label>
                    <input value="<?php echo !empty($_POST['name']) ? $_POST['name'] : '';?>" class="form-control validate[required] inputbg" placeholder="" type="text" name="name" id="Name" required />
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Email*</label>
                    <input value="<?php echo !empty($_POST['email']) ? $_POST['email'] : '';?>" class="form-control validate[required, custom[email]] inputbg" placeholder="" type="email" name="email" id="Email" required />
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Inquiry*</label>
                    <textarea name="comments" id="Comments" placeholder="" class="form-control validate[required] textbg" rows="4" style="height:80px; font-size:13px;" required ><?php echo !empty($_POST['comments']) ? $_POST['comments'] : '';?></textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">How much is <?php echo $verify_robot[0].' + '.$verify_robot[1]?>*</label>
                    <input value="<?php echo !empty($_POST['captcha']) ? $_POST['captcha'] : '';?>" class="form-control validate[required, custom[email]] inputbg" placeholder="" type="text" name="captcha" id="captcha" required />
                </div>



                <button type="submit" class="sent-btn">Submit</button>

            </form>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <p><span class="red-text">EMAIL</span><br>
                <a href="mailto:<?php echo (defined('CONTACT_EMAIL')) ? CONTACT_EMAIL : '';?>"><?php echo (defined('CONTACT_EMAIL')) ? CONTACT_EMAIL : '';?></a></p>
            <!--<p><span class="red-text">TELEPHONE</span><br>
                <?php /*echo (defined('CONTACT_TELEPHONE')) ? CONTACT_TELEPHONE : '';*/?></p>
            <p><span class="red-text">ADDRESS</span><br>
                <?php /*echo (defined('CONTACT_ADDRESS')) ? nl2br(CONTACT_ADDRESS) : '';*/?>
            </p>-->
        </div><!-- /.col-lg-4 -->
        <?php
    } else if ($show404 != true) {?>
        <div class="cms_page_content_wrap">
            <?php echo stripslashes($cmsData->description); ?>
        </div>
    <?php } else {?>
        <div class="col-lg-12">
            <div class="page-not-found">
                <p class="error-code">404</p>
                <p class="error-message">Page not found</p>
                <p class="error-message+"><a href="<?php echo ROOT_URL;?>">Click here to go to home page</a></p>
            </div>
        </div>
    <?php }?>
</div>
