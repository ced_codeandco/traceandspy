<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/style.css">
<?php if (!empty($what_we_do_list)) {
    $i=1;
    foreach ($what_we_do_list as $what_we_do) {
        $section_class = 'intro-section relationship-cheating';
        if ($i > 1) {
            $section_class = ($i % 2 == 0) ? 'intro-section protect-children' : 'intro-section monitor-workers';
        }
        $background_img = '';
        if (!empty($what_we_do->additional_image_1) && file_exists(DIR_UPLOAD_BANNER.$what_we_do->additional_image_1)) {
            $background_img = DIR_UPLOAD_BANNER_SHOW.$what_we_do->additional_image_1;
        }
        ?>
        <section class="<?php echo $section_class;?> home_item_list_wrap" style="<?php if (!empty($background_img)){echo "background-image: url($background_img)";}?> ">
            <div class="container">
                <div class="row">
                    <?php if ($i % 2 == 1) {?>
                        <div class="col-md-8">

                            <h2><?php echo $what_we_do->title;?></h2>
                            <p><?php echo $what_we_do->home_description;?></p>
                            <button data-url="<?php echo ROOT_URL.$what_we_do->cms_slug;?>" class="ar-btn home-product-nav-control">Learn How</button>


                        </div>

                        <div class="col-md-4">
                            <!-- empty element -->
                        </div>
                    <?php } else {?>
                        <div class="col-md-6">
                            <!-- empty element -->
                        </div>
                        <div class="col-md-6">

                            <h2><?php echo $what_we_do->title;?></h2>
                            <p><?php echo $what_we_do->home_description;?></p>
                            <button data-url="<?php echo ROOT_URL.$what_we_do->cms_slug;?>" class="ar-btn home-product-nav-control">Learn How</button>

                        </div>
                    <?php }?>
                </div>
            </div>
        </section><?php
        $i++;
    }
}

if (!empty($contactUsData)) {
    $background_img = '';
    if (!empty($contactUsData->additional_image_1) && file_exists(DIR_UPLOAD_BANNER.$contactUsData->additional_image_1)) {
        $background_img = "url(".DIR_UPLOAD_BANNER_SHOW.$contactUsData->additional_image_1.")";
    }?>
<section class="intro-section discover-truth home_item_list_wrap" style="<?php if (!empty($background_img)){echo "background-image: $background_img";}?>">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <!-- empty element -->
            </div>

            <div class="col-md-6">

                <h2><?php echo $contactUsData->home_title;?></h2>
                <h5><?php echo $contactUsData->home_sub_title;?></h5>
                <p><?php echo $contactUsData->home_description;?></p>
                <button data-url="<?php echo ROOT_URL.$contactUsData->cms_slug;?>" class="ar-btn home-product-nav-control"><?php echo $contactUsData->title;?></button>

            </div>


        </div>
    </div>
</section><?php
}
