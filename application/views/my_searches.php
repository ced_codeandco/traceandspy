<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 4/23/2015
 * Time: 11:06 AM
 */?>
<div>
    <?php
    if(isset($errMsg) && $errMsg != ''){ ?>
        <div class="alert alert-danger">
            <?php echo $errMsg;?>
        </div>
        <?php unset($errMsg);
    }
    if(isset($succMsg) && $succMsg != ''){ ?>
        <div class="alert alert-success">
            <?php echo $succMsg;?>
        </div>
        <?php unset($succMsg);
    }?>
    <?php if(empty($searchList)) {?>
    <div class="col-xs-12">
        <p>You have not saved any searches.</p>
        <p>Try searching at <a href="<?php echo ROOT_URL?>">Home page</a></p>
    </div>
    <?php } else {?>
        <?php echo $paginator;?>
    <div class="col-xs-12">
        <ul class="price-list">
        <?php foreach ($searchList as $search) {?>
            <li>
            <p class="inside-link search-string">
            <?php echo searchQueryToTags(ROOT_URL.'search?'.urldecode($search->search_query));?>
            </p>
            <span class="saved-search-links"><a href="<?php echo ROOT_URL.'search?'.urldecode($search->search_query);?>">View results</a> </span>
            <span onclick="return confirm('Do you really want to remove this search ?')" class="saved-search-links"><a href="<?php echo MEMBER_ROOT_URL.'dashboard/delete_saved_search/'.$search->id;?>">Delete this search</a> </span>
            </li><?php
        }
        ?>
        </ul>
    </div>
        <?php echo $paginator;?>
    <?php }?>
</div>