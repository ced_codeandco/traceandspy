<?php if ($show404 != true) {
    //print_r($productData);?>
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
        <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
        <header class="bp-banner discover-bnr parallax-top-bnr">

            <div class="wrapper">

                <div class="bnr-content">
                    <h1><?php echo $productData->title;?></h1>
                    <p><?php echo $productData->small_description;?></p>

                </div>

            </div>
        </header>
            <?php
            $cms_wrapper = 'discover-cheating';?>
            <section class="<?php echo $cms_wrapper;?>">
                <div class="container product_details_wrap">
                    <div class="add_to_cart">
                        <a href="<?php echo ROOT_URL.'buy/'.$productData->id;?>">Buy Now</a>
                    </div>
                    <div class="clearfix"></div>
                    <?php if (!empty($productData->product_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$productData->product_banner_image) && file_exists(DIR_UPLOAD_BANNER.$productData->product_banner_image)) {?>
                            <img class="inline-product-image" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$productData->product_banner_image;?>" />
                    <?php }?>
                    <?php echo $productData->description;?>

                </div>
            </section>

        <?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
            <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
        <?php }?>
<?php } else {?>

<?php } ?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
$(function(){
    if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
        $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
    }
});
</script>
