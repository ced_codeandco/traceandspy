<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="row">
    <div class="col-lg-12">
        <ul class="bradcram">
            <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
            <li>Register</li>
        </ul>
        <h3 class="text-left">Select a package</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">




        <div id="myTabContent" class="tab-content ">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom">
                    <strong>Select a package from the list below.</strong>
                </p>

                <div class="col-lg-13 peding-left-none">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form method="post" action=""  name="registerForm" id="registerForm" class="profile-lable">
                        <input type="hidden" name="user_id" value="<?php echo $member_id;?>">
                        <?php if (!empty($package_list) && is_array($package_list)) {?>
                            <table class="table_package">
                                    <tr>
                                        <th class="package-table-col-1">Package</th>
                                        <th class="package-table-col-2">Benefits</th>
                                        <th class="package-table-col-3">Price USD</th>
                                        <th class="package-table-col-4">Select Package</th>
                                    </tr>
                                    <?php

                                    $i = 0;
                                    foreach ($package_list as $package) {//classified_slug ?>
                                        <tr class="package_row">
                                            <td class="package-table-col-1"><?php echo $package->title; ?></td>
                                            <td class="package-table-col-2"><?php echo $package->description; ?></td>
                                            <td class="package-table-col-3"><?php echo $package->price; ?></td>
                                            <td class="package-table-col-4">
                                                <input type="radio" name="package_id" id="package_selector_<?php echo $package->id;?>" value="<?php echo $package->id;?>">
                                                <?php if (!empty($package->price) && $package->price > 0){?>
                                                <input type="hidden" name="package_name" id="package_name_<?php echo $package->id;?>" value="<?php echo $package->title;?>">
                                                <input type="hidden" name="package_price" id="package_price_<?php echo $package->id;?>" value="<?php echo $package->price;?>">
                                                <?php }?>
                                            </td>
                                        </tr>
                                        <?php


                                    }?>
                            </table>
                            <div class="package_head">
                                <div class="tit"><label id="package_id-error" class="error package_error" for="package_id">Please select a package</label></div>
                            </div><?php
                        }?>
                        <div class="clearfix"></div>
                        <button type="submit" class="update-profile-btn" id="shop_at_checkout">Continue</button>
                    </form>


                    <!--<form action='<?php /*echo $two_co_post_url;*/?>' method='post' id="two_co_form">

                        <input type='hidden' name='sid' value='<?php /*echo $two_co_account_number;*/?>' />
                        <input type='hidden' name='mode' value='2CO' />
                        <?php /*if (!empty($two_co_account_mode) && $two_co_account_mode =='demo') {*/?>
                            <input type='hidden' name='demo' value='Y' />
                        <?php /*}*/?>
                        <input type='hidden' name='li_0_name' value='' />
                        <input type='hidden' name='li_0_price' value='' />
                        <input type='hidden' name='li_0_tangible' value='N' />
                        <input type='hidden' name='custom_username' value='jdoe0123' />
                        <input type='hidden' name='package_id' value='id5584762' />

                    </form>-->


                    <div class="clearfix"></div>

                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {

        $('.package_row').click(function(){
            $('.package_row').removeClass('selected');
            $(this).find('td > input[type="radio"]').prop('checked', true);
            $(this).addClass('selected');
            var package_id = $(this).find('td > input[type="radio"]').val();
            var package_name = $(this).find('td > input[name="package_name"]').val();
            var package_price = $(this).find('td > input[name="package_price"]').val();
            //alert(package_id +' - '+ package_name +' - '+ package_price);

            $('#two_co_form > input[name="li_0_name"]').val(package_name);
            $('#two_co_form > input[name="li_0_price"]').val(package_price);
            $('#two_co_form > input[name="package_id"]').val(package_id);
        })

        $('#registerForm').validate({
            rules: {
                package_id:{required: true},
            },
            messages: {
                package_id:{required: 'Please select a package'},
            }
        })
    });
</script>