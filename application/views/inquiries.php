<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 4/23/2015
 * Time: 11:18 AM
 */
?>
<div class="full-widht">
    <div >
        <div class="col-xs-12">
            <p>
                <strong><?php echo $profileData->first_name. ' ' .$profileData->last_name?></strong>
                <span class="font-12px">(not <?php echo $profileData->first_name;?>? <a href="<?php echo MEMBER_ROOT_URL?>logout" >Logout</a>)</span>
            </p>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12"><img src="<?php echo ROOT_URL_BASE?>images/profile-dummy-large.png" class="img-thumbnail"></div>
        <div class="col-lg-5 col-md-6 col-xs-12">
            <form name="" method="post" action="">
                <div class="bs-example" data-example-id="simple-table">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <table class="table">
                        <caption><!--Optional table caption.--></caption>
                        <thead>
                        <tr>
                            <th>Name:</th>
                            <th> <?php echo $profileData->first_name. ' ' .$profileData->last_name?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>First name:</td>
                            <td>
                                <input class="form-control" type="text" id="exampleInputEmail1" placeholder="First Name" required name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>" >
                            </td>
                        </tr>
                        <tr>
                            <td>Last name:</td>
                            <td>
                                <input class="form-control" type="text" id="exampleInputEmail1" placeholder="Last Name" required name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>" >
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address / Username" required name="email" value="<?php echo !empty($formData['email']) ? $formData['email'] : ''?>"
                                       onblur="validate_user_name(this, '<?php echo ROOT_URL.'register/check_duplicate_email';?>', 'p', 'alert-danger')"
                                    >
                            </td>
                        </tr>
                        <tr>
                            <td>Gender:</td>
                            <td>
                                <div class="">
                                    <input id="radio1" type="radio" required name="gender" value="Male" <?php echo (!empty($formData['gender']) && strtolower($formData['gender']) == strtolower('Male')) ? 'checked="checked"' : ''?>>
                                    <label for="radio1"><span><span></span></span>Male</label>
                                    <input id="radio2" type="radio" required name="gender" value="Female" <?php echo (!empty($formData['gender']) && strtolower($formData['gender']) == strtolower('Female')) ? 'checked="checked"' : ''?>>
                                    <label for="radio2"><span><span></span></span>Female</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Date of  Birth:</td>
                            <td>
                                <input type="text" class="form-control" id="datePicker" placeholder="Date of Birth" required name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? date('d F Y', strtotime($formData['date_of_birth'])) : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Contact No.:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Contact No." required name="contact_no" value="<?php echo !empty($formData['contact_no']) ? $formData['contact_no'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Company:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Company" name="company" value="<?php echo !empty($formData['company']) ? $formData['company'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Business Type:</td>
                            <td>
                                <div class="select_style">
                                    <select name="business_id">
                                        <option value="">Business Type</option>
                                        <?php if (is_array($businessList)) {
                                            foreach ($businessList as $business) {
                                                echo '<option value="' .$business->id. '" ' .((!empty($formData['business_id']) && $formData['business_id'] == $business->id) ? 'selected="selected"' : ''). '>' .$business->name. '</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Address 1:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Address 1" required name="address_1" value="<?php echo !empty($formData['address_1']) ? $formData['address_1'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Address 2:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Address 2" name="address_2" value="<?php echo !empty($formData['address_2']) ? $formData['address_2'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>
                                <div class="select_style">
                                    <select required name="country">
                                        <option value="">Country</option>
                                        <?php if (is_array($countryList)) {
                                            foreach ($countryList as $country) {
                                                echo '<option value="' .$country->id. '" ' .((!empty($formData['country']) && $formData['country'] == $country->id) ? 'selected="selected"' : ''). '>' .$country->name. '</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>State:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="State" name="state" value="<?php echo !empty($formData['state']) ? $formData['state'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="City" name="city" value="<?php echo !empty($formData['city']) ? $formData['city'] : ''?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Postal code:</td>
                            <td>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Postal code" name="postal_code" value="<?php echo !empty($formData['postal_code']) ? $formData['postal_code'] : ''?>">
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="col-md-4 col-sm-4 col-xs-12 pedding-none">
                        <button type="submit" class="btn change-passward">Update Profile</button>
                    </div>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>

                </div>
            </form>

        </div>
        <div class="col-lg-5 col-md-4 col-xs-12">
            <div class="col-lg-4 col-md-4 col-xs-12 ">
                <p class="text-center font-12px-grey">
                    <strong>My Ads</strong><br>
                    <strong><?php echo $dashboardNotification['myAdCount'];?></strong><br>
                    <!--ads viewed 0 times-->
                </p>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 border-left">
                <p class="text-center font-12px-grey">
                    <strong>My Searches</strong><br>
                    <strong><?php echo $dashboardNotification['savedSearchCount'];?></strong><br>saved searches
                </p>
            </div>
            <!--<div class="col-lg-4 col-md-4 col-xs-12"><p class="text-center font-12px-grey"><strong>My Watchlist</strong><br>
                    <strong>0</strong><br>
                    ads saved</p></div>-->

        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo ROOT_URL;?>css/themes/base/jquery.ui.all.css">
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(function() {
        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy'
        });
    });
</script>