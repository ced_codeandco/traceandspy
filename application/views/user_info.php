<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
<header class="bp-banner discover-bnr parallax-top-bnr">

    <div class="wrapper">

        <div class="bnr-content">
            <h1><?php echo $productData->title;?></h1>
            <p><?php echo $productData->small_description;?></p>

        </div>

    </div>
</header>

<?php
$cms_wrapper = 'discover-cheating';?>
<section class="<?php echo $cms_wrapper;?>">
    <div class="container user_info_form">
        <div class="col-lg-10 peding-left-none">
            <?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>
            <?php echo validation_errors(); ?>
            <form method="post" action=""  name="registerForm" id="registerForm" class="profile-lable">
                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Email Address:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" placeholder="username@provider.com" type="text" name="email" value="<?php echo !empty($formData['email']) ? $formData['email'] : ''?>" id="email" onblur="validate_user_name(this, '<?php echo ROOT_URL.'register/check_duplicate_email/'.$productData->id;?>', 'label', 'error')">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Confirm Email Address:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" placeholder="username@provider.com" type="text" name="email_again" value="<?php echo !empty($formData['email_again']) ? $formData['email_again'] : ''?>">
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Password:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password" id="password"  value="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Confirm Password:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password_again" value="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">First Name:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" placeholder="Enter your real name" class="validate[required] inputbg" type="text" name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Last Name:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" placeholder="Your last name will be kept private" class="validate[required] inputbg" type="text" name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>" >
                    </div>
                </div>

                <div class="clearfix"></div>


                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Date Of Birth:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" id="datePicker" placeholder="Date of birth" class="validate[required] inputbg" type="text" name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? $formData['date_of_birth'] : ''?>" >
                    </div>
                </div>


                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">Contact Number:</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <input  class="form-control" placeholder="Contact Number" type="text" name="contact_no" value="<?php echo !empty($formData['contact_no']) ? $formData['contact_no'] : ''?>">
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 peding-left-none">
                        <label for="exampleInputEmail1">&nbsp;</label>
                    </div>
                    <div class="col-lg-9 col-sm-9  peding-left-none">
                        <button type="submit" class="update-profile-btn">Register</button>
                    </div>
                </div>
            </form>



        </div>
    </div>
</section>

<?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
<?php }?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
    $(function(){
        if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
            $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
        }
    });
</script>



</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {

        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear(),
            maxDate: "-15Y"
        });
        /*$( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });*/

        $.validator.addMethod("customvalidation",
            function(value, element) {
                return /^[A-Za-z\d=#$%@_ -]+$/.test(value);
            },
            "Sorry, no special characters allowed"
        );
        $.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number");
        $('#registerForm').validate({
            rules: {
                email:{required: true, email:true, maxlength:100},
                email_again:{required: true, email:true, equalTo: '#email', maxlength:100},
                password:{required: true, maxlength:100, minlength:5},
                password_again:{required: true, maxlength:100 ,equalTo: '#password'},
                first_name: {required: true, maxlength:100},
                last_name: {required: true, maxlength:100},
                security_question:{required: true},
                security_answer:{required: true, maxlength:100, minlength:5 },
                date_of_birth:{required: true, date:true},
                contact_no:{required: true, maxlength: 15, validPhoneNumber: true, maxlength:15},
                country:{required: true},
                city:{required: true},
                age_confirmation:{required: true},
            },
            messages: {
                first_name: {required: 'Please enter your first name'},
                last_name: {required: 'Please enter your last name'},
                email:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID'},
                email_again:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID', equalTo:'Email mismatch'},
                password:{required: 'Please enter a password', minlength:'Your password should contain at least 5 character'},
                password_again:{required: 'Please confirm your password', equalTo: 'Password mismatch'},
                security_question:{required: 'Please select a security question'},
                security_answer:{required: 'Please enter your answer to security  question' },
                /*gender:{required: 'Please select your gender'},*/
                date_of_birth:{required: 'Please enter your date of birth'},
                contact_no:{required: 'Please enter your contact number', maxlength: 'Invalid phone number', validPhoneNumber: 'Invalid phone number'},
                address_1:{required: 'Address 1 is required'},
                city:{required: 'Please enter your city'},
                country:{required: 'Please select your country'},
                age_confirmation:{required: 'Please confirm your age'},
            }
        })
    });
</script>