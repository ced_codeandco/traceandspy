<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
<header class="bp-banner discover-bnr parallax-top-bnr">

    <div class="wrapper">

        <div class="bnr-content">
            <h1><?php echo $cmsData->title;?></h1>
            <p><?php echo $cmsData->small_description;?></p>

        </div>

    </div>
</header>

<?php
$cms_wrapper = 'discover-cheating';?>
<section class="<?php echo $cms_wrapper;?>">
    <div class="container user_info_form">
        <div class="col-lg-10 peding-left-none">
        <?php
        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
        $loginUrl = ROOT_URL.'login'.(!empty($_GET['back']) ? '?back='.$_GET['back'] : '');
        echo form_open($loginUrl, $attributes); ?>
        <?php
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
            <div class="form-group">
                <label for="exampleInputEmail1">Email Address*</label>
                <input type="email" required="required" name="username" id="username" placeholder="username@provider.com" value="<?php echo !empty($_POST['username']) ? $_POST['username'] : ''?>" class="form-control" >
            </div>
            <div class="form-group">
                <p class="posword-box">Password <a href="<?php echo ROOT_URL;?>forgot_password" class="text-right">Forgot Password?</a></p>
                <input type="password" class="form-control" name="password" id="password">
            </div>


            <button type="submit" class="sign-in">Sign in</button>
        <div class="clearfix"></div>
        <?php /*<p>Don't have an account ? <a href="<?php echo ROOT_URL;?>register" class="red-text">Register</a></p>*/?>
        </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
        </div>
    </div>
</section>
<?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
<?php }?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
    $(function(){
        if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
            $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
        }
    });
</script>