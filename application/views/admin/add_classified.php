<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> File Page</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> File Page</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php
          //print_r($classifiedDetails);
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$classifiedDetails->id;
	}
	
	$attributes = array('name' => 'classifiedForm', 'id' => 'classifiedForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_classified();');
				echo form_open(ADMIN_ROOT_URL.'classified/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($classifiedDetails->id)) ? $classifiedDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />

          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($classifiedDetails->title)) ? $classifiedDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>



            <div class="form-group input-group col-md-4" id="file_name_msg_error">
                <label for="file_name">Upload file</label><br />
                <input type="file" name="file_name" id="file_name" class="input-text-02"   />
                <?php if(isset($classifiedDetails->file_name) && $classifiedDetails->file_name!='' && file_exists(DIR_UPLOAD_FILE.$classifiedDetails->file_name)) {?>
                    <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $classifiedDetails->file_name;  ?>" />
                <?php } ?>

            </div>




          <div class="control-group">
            <label class="control-label" for="selectError">File Uploaded By<span class="required">*</span></label>
            <div class="controls">
              <select id="created_by" name="created_by" data-rel="chosen"> 
			  	<?php foreach($memberList as $member){ ?>
				 <option value="<?php echo $member->id?>" <?php echo (isset($classifiedDetails->created_by) && $classifiedDetails->created_by== $member->id) ? "selected='selected'" : "" ?> ><?php echo $member->first_name. ' ' .$member->last_name?></option>
				<?php }?>
              </select>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($classifiedDetails->is_active) && $classifiedDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                <option value="0" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($classifiedDetails->is_active) && $classifiedDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >In Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_classified(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter File title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
    /*if($("#small_description").val()==''){
		$("#small_description_msg").html('Please enter Classified Short Description');
		$("#small_description_msg_error").addClass('has-error');
		$("#small_description").focus();
		return false;
	}else{
		$("#small_description_msg").html('');
		$("#small_description_msg_error").removeClass('has-error');
	}
	if($("#brand").val()==''){
		$("#brand_msg").html('Please enter brand');
		$("#brand_msg_error").addClass('has-error');
		$("#brand").focus();
		return false;
	}else{
		$("#brand_msg").html('');
		$("#brand_msg_error").removeClass('has-error');
	}*/
	/*if($("#model").val()==''){
		$("#model_msg").html('Please enter model');
		$("#model_msg_error").addClass('has-error');
		$("#model").focus();
		return false;
	}else{
		$("#model_msg").html('');
		$("#model_msg_error").removeClass('has-error');
	}*/
	if($("#manufacture_year").val()==''){
		$("#manufacture_year_msg").html('Please enter Manufacture Year');
		$("#manufacture_year_msg_error").addClass('has-error');
		$("#manufacture_year").focus();
		return false;
	}else{
		$("#manufacture_year_msg").html('');
		$("#manufacture_year_msg_error").removeClass('has-error');
	}
	if($("#classified_contact_no").val()==''){
		$("#classified_contact_no_msg").html('Please enter File Contact Number');
		$("#classified_contact_no_msg_error").addClass('has-error');
		$("#classified_contact_no").focus();
		return false;
	}else{
		$("#classified_contact_no_msg").html('');
		$("#classified_contact_no_msg_error").removeClass('has-error');
	}
	/*if($("#classified_contact_name").val()==''){
		$("#classified_contact_name_msg").html('Please enter Classified Contact Name');
		$("#classified_contact_name_msg_error").addClass('has-error');
		$("#classified_contact_name").focus();
		return false;
	}else{
		$("#classified_contact_name_msg").html('');
		$("#classified_contact_name_msg_error").removeClass('has-error');
	}*/
	/*if($("#classified_contact_email").val()==''){
		*//*$("#classified_contact_email_msg").html('Please enter Classified Contact Email');
		$("#classified_contact_email_msg_error").addClass('has-error');
		$("#classified_contact_email").focus();
		return false*//*;
	}else{
		$("#classified_contact_email_msg").html('');
		$("#classified_contact_email_msg_error").removeClass('has-error');
	}*/
	/*if($("#classified_land_mark").val()==''){
		$("#classified_land_mark_msg").html('Please enter Classified Landmark');
		$("#classified_land_mark_msg_error").addClass('has-error');
		$("#classified_land_mark").focus();
		return false;
	}else{
		$("#classified_land_mark_msg").html('');
		$("#classified_land_mark_msg_error").removeClass('has-error');
	}
	if($("#classified_address").val()==''){
		$("#classified_address_msg").html('Please enter Classified Address');
		$("#classified_address_msg_error").addClass('has-error');
		$("#classified_address").focus();
		return false;
	}else{
		$("#classified_address_msg").html('');
		$("#classified_address_msg_error").removeClass('has-error');
	}*/
}
</script> 
