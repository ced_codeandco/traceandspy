<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">View - Edit Details</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> View - Edit Details</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	
	$attributes = array('name' => 'adminForm', 'id' => 'adminForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_admin();');
				echo form_open(ADMIN_ROOT_URL.'admin_profile',$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($adminDetails->id)) ? $adminDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="form-group input-group col-md-4" id="first_name_msg_error">
            <label class="control-label" for="first_name">First name</label>
            <input type="text" class="form-control" maxlength="255" name="first_name" value="<?php if(isset($_SESSION['first_name']) && $_SESSION['first_name'] != '') { echo $_SESSION['first_name']; unset($_SESSION['first_name']);}else { echo (isset($adminDetails->first_name)) ? $adminDetails->first_name : ''; }?>" id="first_name" placeholder="Enter First Name">
            <br />
            <label class="control-label" id="first_name_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="last_name_msg_error">
            <label for="last_name">Last name</label>
            <input type="text" class="form-control"  maxlength="255" name="last_name" value="<?php echo (isset($adminDetails->last_name)) ? $adminDetails->last_name : '';?>" id="last_name" placeholder="Enter Last Name">
          </div>
          <div class="form-group input-group col-md-4" id="email_msg_error">
            <label for="email">Email Address / Username</label>
            <input type="email" class="form-control" maxlength="255" name="email" value="<?php echo (isset($adminDetails->email)) ? $adminDetails->email : '';?>" id="email" placeholder="Enter Email address / Username">
            <br />
            <label class="control-label" id="email_msg"></label>
          </div>
          <?php /* if($action == 'Add'){ ?>
          <div class="form-group input-group col-md-4" id="password_msg_error">
            <label for="password">Password</label>
            <input type="password" class="form-control" maxlength="255" name="password" value="" id="password" placeholder="Enter Password">
            <br />
            <label class="control-label" id="password_msg"></label>
            <span id="loading_pass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
          <div class="form-group input-group col-md-4" id="retype_password_msg_error">
            <label for="retype_password">Password again</label>
            <input type="password" class="form-control" maxlength="255" name="retype_password" value="" id="retype_password" placeholder="Enter Password again">
            <br />
            <label class="control-label" id="retype_password_msg"></label>
            <span id="loading_repass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
          <?php } */ ?>
          
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active : <?php echo (isset($adminDetails->is_active) && $adminDetails->is_active==1) ? "Active" : "In Active" ?> </label>
            
          </div>
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_admin(){
	
	if($("#first_name").val()==''){
		$("#first_name_msg").html('Please enter first name');
		$("#first_name_msg_error").addClass('has-error');
		$("#first_name").focus();
		return false;
	}else{
		$("#first_name_msg").html('');
		$("#first_name_msg_error").removeClass('has-error');
	}
	
	if($("#email").val()==''){
		$("#email_msg").html('Please enter Email address');
		$("#email_msg_error").addClass('has-error');
		$("#email").focus();
		return false;
	}else if(!validateEmailRegexp($("#email").val()) ){
		
		$("#email_msg").html('Please enter valid Email address');
		$("#email_msg_error").addClass('has-error');
		$("#email").focus();
		return false;
	}else{
		$("#email_msg").html('');
		$("#email_msg_error").removeClass('has-error');
	}
/*
		var password = $("#password").val();
	
		var passed = validatePassword(password, {
	
			length:   [6, Infinity],
	
			numeric:  1,
	
			special:  1
	
		});
	
		
	
		if($("#password").val()==''){
	
			$("#password_msg").html('Please enter Password');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
			
	
		}else if(!passed){
	
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char.');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
		}else if($("#password").val()!='' && $("#password").val()!=$("#retype_password").val()){
	
			$("#retype_password_msg").html('Password and Again password do not matched');
	
			$("#retype_password_msg_error").addClass('has-error');
	
			$("#retype_password").focus();
	
			return false;
	
		}else{
	
			$("#password_msg").html('');
	
			$("#password_msg_error").removeClass('has-error');
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	
		}
	*/
	
}
$(document).ready(function(){
	
	$("#password").blur(function (){
		$("#loading_pass").show();
		var password = $("#password").val();
		var passed = validatePassword(password, {
			length:   [6, Infinity],
			numeric:  1,
			special:  1
		});
		
		if($("#password").val()==''){
			$("#password_msg").html('Please enter Password');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else if(!passed){
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char. ');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else{
			$("#password_msg_error").removeClass('has-error');
			$("#loading_pass").hide();
			$("#password_msg").html('');
  		}
  		
 	});
	$("#retype_password").blur(function (){
		$("#loading_repass").show();
		if($("#retype_password").val()!==$("#password").val()){
			$("#retype_password_msg_error").addClass('has-error');
			$("#retype_password_msg").html('Password and Again password do not matched');
	  		$("#loading_repass").hide();
	  	}else{
			
			
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	  		$("#loading_repass").hide();
  		}
	});
});
</script> 
