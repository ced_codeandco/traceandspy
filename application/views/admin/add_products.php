<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#"><?php echo $action;?> Products</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Products</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$productDetails->id;
                        }

                        $attributes = array('name' => 'productForm', 'id' => 'productForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_product();');
                        echo form_open('',$attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($productDetails->id)) ? $productDetails->id : 0;?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />


                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($productDetails->title)) ? $productDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>
                        <div class="form-group input-group col-md-4" id="small_description_msg_error">
                            <label for="small_description">Sub title</label>
                            <textarea class="form-control"  maxlength="500" name="small_description"  id="small_description" placeholder="Short Description"><?php if(isset($_SESSION['small_description']) && $_SESSION['small_description'] != '') { echo $_SESSION['small_description']; unset($_SESSION['small_description']);}else { echo (isset($productDetails->small_description)) ? $productDetails->small_description : ''; }?></textarea>
                        </div>

                        <div class="form-group input-group col-md-4" id="price_msg_error">
                            <label class="control-label" for="price">Price (USD)<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="price" value="<?php if(isset($_SESSION['price']) && $_SESSION['price'] != '') { echo $_SESSION['price']; unset($_SESSION['price']);}else { echo (isset($productDetails->price)) ? $productDetails->price : ''; }?>" id="price" placeholder="Enter Price">
                            <br />
                            <label class="control-label" id="price_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="bullet_description_msg_error">
                            <label class="control-label" for="bullet_description">Bullet points on listing page<span class="required">*</span></label>
                            <?php if(isset($_SESSION['bullet_description']) && $_SESSION['bullet_description'] != '') {
                                $bullet_description = $_SESSION['bullet_description']; unset($_SESSION['bullet_description']);
                            } else {
                                $bullet_description = (isset($productDetails->bullet_description)) ? $productDetails->bullet_description : '';
                            }
                            $bullet_description = !empty($bullet_description) ? unserialize($bullet_description) : array();
                            ?>
                            <?php if (!empty($bullet_description) && is_array($bullet_description)) {
                                foreach ($bullet_description as $bullet) {
                                    if (!empty($bullet)) { ?>
                                        <input type="text" class="form-control" maxlength="255"
                                               name="bullet_description[]" value="<?php echo $bullet; ?>"
                                               id="bullet_description" placeholder="Enter Points">
                                    <?php }
                                }
                            } else {?>
                                <input type="text" class="form-control" maxlength="255"
                                       name="bullet_description[]" value=""
                                       id="bullet_description" placeholder="Enter Points">
                            <?php
                            }?>
                            <input type="button" value="+" onclick="add_more_bullets(this);" price="Add more points">
                            <br />
                            <label class="control-label" id="bullet_description_msg"></label>
                        </div>



                        <div class="form-group input-group col-md-4" id="description_msg_error">
                            <label for="description">Description</label><br />
                            <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($productDetails->description)) ? $productDetails->description : ''; }?>
                            <?php echo $this->ckeditor->editor("description",stripslashes($description));?>
                        </div>

                        <div class="form-group input-group col-md-4" id="product_banner_image_msg_error">
                            <label for="product_banner_image">Product Image</label> <span> (Preferred size: 583 X 482 pixel)</span><br />
                            <input type="file" name="product_banner_image" id="product_banner_image" class="input-text-02"   />
                            <?php if(isset($productDetails->product_banner_image) && $productDetails->product_banner_image!='' && file_exists(DIR_UPLOAD_BANNER.$productDetails->product_banner_image)) {?>
                                <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$productDetails->product_banner_image ?>&q=100&w=300"/>
                                <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $productDetails->product_banner_image;  ?>" />
                            <?php } ?>
                        </div>

                        <div class="form-group input-group col-md-4" id="meta_title_msg_error">
                            <label for="meta_title">Meta Title</label>
                            <textarea class="form-control"  maxlength="255" name="meta_title"  id="meta_title" placeholder="Meta Title"><?php if(isset($_SESSION['meta_title']) && $_SESSION['meta_title'] != '') { echo $_SESSION['meta_title']; unset($_SESSION['meta_title']);}else { echo (isset($productDetails->meta_title)) ? $productDetails->meta_title : ''; }?></textarea>
                        </div>

                        <div class="form-group input-group col-md-4" id="meta_keywords_msg_error">
                            <label for="meta_keywords">Meta Keywords</label>
                            <textarea class="form-control"  maxlength="255" name="meta_keywords"  id="meta_keywords" placeholder="Meta Keywords"><?php if(isset($_SESSION['meta_keywords']) && $_SESSION['meta_keywords'] != '') { echo $_SESSION['meta_keywords']; unset($_SESSION['meta_keywords']);}else { echo (isset($productDetails->meta_keywords)) ? $productDetails->meta_keywords : ''; }?></textarea>

                        </div>

                        <div class="form-group input-group col-md-4" id="meta_desc_msg_error">
                            <label for="meta_desc">Meta Description</label>
                            <textarea class="form-control"  maxlength="255" name="meta_desc"  id="meta_desc" placeholder="Meta Description"><?php if(isset($_SESSION['meta_desc']) && $_SESSION['meta_desc'] != '') { echo $_SESSION['meta_desc']; unset($_SESSION['meta_desc']);}else { echo (isset($productDetails->meta_desc)) ? $productDetails->meta_desc : ''; }?></textarea>

                        </div>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="0" selected="selected">In Active</option>
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($productDetails->is_active) && $productDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                </select>
                            </div>
                        </div>


                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate_product(){
            if($("#title").val()==''){
                $("#title_msg").html('Please enter products title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
            var price = Number($("#price").val());
            if($("#price").val()=='' || price == 0){
                $("#price_msg").html('Please enter products price');
                $("#price_msg_error").addClass('has-error');
                $("#price").focus();
                return false;
            }else{
                $("#price_msg").html('');
                $("#price_msg_error").removeClass('has-error');
            }
        }
    </script>
