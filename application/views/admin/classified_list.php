<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="<?php echo ADMIN_ROOT_URL?>classified">File List</a> </li>
    <?php if (!empty($owner_details->first_name)){?>
        <li> <a href="#"><?php echo $owner_details->first_name;?></a> </li><?php
    }?>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> File List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="7%">No</th>
          <th width="13%">Title</th>
          <th width="10%">Member Name</th>
          <th width="14%" style="text-align:center">Created Date</th>
          <th width="8%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
			 
		$i = 0;
		if($classifiedList && count($classifiedList) > 0 ){
			$paOrder =1; 

		foreach ($classifiedList as $classified){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?></td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>classified/add/<?php echo $classified->id?>" ><?php echo $classified->title;  ?></a>
                     </td>
          <td><a href="<?php echo ADMIN_ROOT_URL?>classified/index/mem-<?php echo $classified->created_by?>" ><?php echo $classified->member_name;  ?></a></td>


            
          <td style="text-align:center"><?php echo date('d-m-Y, H:i A', strtotime($classified->created_date_time));  ?></td>
          <td style="text-align:center" id="td_status_<?php echo $classified->id ?>">
          
           
            <?php if($classified->file_status=='2'){?>
                <a href="<?php echo ADMIN_ROOT_URL?>classified/status_inactive/<?php echo $classified->id?>" class="label-success label label-default" >Converted</a>
            <?php }else{?>
                <a href="<?php echo ADMIN_ROOT_URL?>classified/status_active/<?php echo $classified->id?>" class="label-default label label-danger"  >In Progress</a>
            <?php }?>
           
          </td>
                  
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>classified/add/<?php echo $classified->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
            
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>classified/delete/<?php echo $classified->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            <?php if($classified->file_status == '1'){?>
                <a href="<?php echo ROOT_URL?>download_source_file/<?php echo $classified->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-download-alt icon-white"></i> Uploaded file</a>
            <?php } else if($classified->file_status == '2'){?>
                <a href="<?php echo ROOT_URL?>download_file/<?php echo $classified->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-download-alt icon-white"></i> Converted File</a>
            <?php }?>
           </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderFile(id,classified_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>classified/order?id="+id+"&classified_order="+classified_order+"&position="+position+"&parent="+parent;
}

</script>