<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Add Image </a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i>  Add Image For <?php echo $classifiedDetails->title?></h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	
		$editUrl = '/'.$classifiedDetails->id;
	
	
	$attributes = array('name' => 'imageForm', 'id' => 'imageForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_image();');
				echo form_open(ADMIN_ROOT_URL.'classified/add_image'.$editUrl,$attributes); ?>
         
          <input type="hidden" name="action" id="action" value="Add" />
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
         
         
            <label class="control-label" for="title">Classified title : <?php echo $classifiedDetails->title?></label>
            
             <input type="hidden" class="form-control" maxlength="255" name="classified_id" value="<?php echo $classifiedDetails->id;?>" id="classified_id">
          </div>
          
          
          <div class="form-group input-group col-md-4" id="classified_image_msg_error">
            <label class="control-label" for="classified_image">Image</label><br />
                       
             <input type="file" name="classified_image" id="classified_image" class="input-text-02"   />
     
        <label class="control-label" id="classified_image_msg"></label>
          </div>
          
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($imageDetails->is_active) && $imageDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_image(){	
	if($("#classified_image").val()==''){
		$("#classified_image_msg").html('Please Select Image');
		$("#classified_image_msg_error").addClass('has-error');
		$("#classified_image").focus();
		return false;
	}else{
		$("#classified_image_msg").html('');
		$("#classified_image_msg_error").removeClass('has-error');
	}
}
</script> 
