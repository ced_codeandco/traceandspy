<footer class="footer">

    <div class="wrapper clearfix">

        <small>&copy; <?php echo date('Y');?>. Trace &amp; Spy, Ltd. <a href="<?php echo ROOT_URL.'cms/'.$terms_popup_content->cms_slug.'/ajax';?>" class="anchor ajax-popup"><?php echo $terms_popup_content->title; ?></a></small>
        <ul class="social-links">
            <li><a target="_blank" href="<?php echo defined('FACEBOOK_PAGE_URL') ? FACEBOOK_PAGE_URL : '';?>"><i class="fa fa-facebook"></i></a></li>
            <li><a target="_blank" href="<?php echo defined('TWITTER_PAGE_LINK') ? TWITTER_PAGE_LINK : '';?>"><i class="fa fa-twitter"></i></a></li>
            <li><a target="_blank" href="<?php echo defined('GOOGLE_PLUS_LINK') ? GOOGLE_PLUS_LINK : '';?>"><i class="fa fa-google-plus"></i></a></li>
        </ul>

    </div>

</footer>




</body>
</html>







<?php return;?>
</div>
<footer class="footer">
    <div class="container">
        <div class="footer-box">
            <ul class="navigation">

                <?php if (!empty($bottom_pages) && is_array($bottom_pages)){
                    foreach($bottom_pages as $page) {
                        if ($page->id == CMS_HOME_PAGE_ID){?>
                            <li><a <?php echo empty($current_slug) ? 'class="active"' : '';?>
                                   href="<?php echo ROOT_URL; ?>"><?php echo $page->title; ?></a>
                            </li><?php
                        } else {
                            $is_popup = '';
                            $page_url = ROOT_URL.$page->cms_slug;
                            if (!empty($page->is_popup) && $page->is_popup == 1) {
                                $is_popup = 'ajax-popup';
                                $page_url = ROOT_URL.'cms/'.$page->cms_slug.'/ajax';
                            }   ?>
                            <li><a class="<?php echo (!empty($current_slug) && $current_slug == $page->cms_slug) ? 'active' : ''; echo ' '.$is_popup;?>"
                                href="<?php echo $page_url; ?>"><?php echo $page->title; ?></a>
                            </li><?php
                        }
                    }
                }?>
                <!--<li><a class="ajax-popup" href="<?php /*echo ROOT_URL.'cms/'.$terms_popup_content->cms_slug.'/ajax';*/?>"><?php /*echo $terms_popup_content->title; */?></a></li>
                <li><a class="ajax-popup" href="<?php /*echo ROOT_URL.'cms/'.$refund_popup_content->cms_slug.'/ajax';*/?>"><?php /*echo $refund_popup_content->title; */?></a></li>-->

            </ul>
            <p>Designed and Developed by <a href="http://codeandco.ae/" target="_blank">Code&Co</a> </p>
        </div>
    </div>
</footer>






<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo ROOT_URL_BASE;?>js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="<?php echo ROOT_URL_BASE;?>js/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo ROOT_URL_BASE;?>js/ie10-viewport-bug-workaround.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/textition.js"></script>
<script type="text/javascript">
    $(window).load(function() {
        $('.flexslider').flexslider();
    });
</script>
</body>
</html>
