<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo defined('DEFAULT_META_TITLE') ? DEFAULT_META_TITLE : SITE_NAME; ?><?php echo !empty($seo['title']) ? ' : '.$seo['title'] : '';?></title>
    <meta name="keywords" content="<?php echo defined('DEFAULT_META_KEYWORDS') ? DEFAULT_META_KEYWORDS : SITE_NAME; ?><?php if (!empty($seo['tags'])) { echo ', '.$seo['tags']; }?>" />
    <meta name="Description" content="<?php if (!empty($seo['description'])){ echo ' '.$seo['description']; } else { echo DEFAULT_META_DESCRIPTION; };?>"/>
    <meta property="og:title" content="<?php echo DEFAULT_META_TITLE;?> <?php echo !empty($seo['title']) ? ' : '.$seo['title'] : '';?>" />
    <meta property="og:description" content="<?php if (!empty($seo['description'])){ echo ' '.$seo['description']; } else {echo DEFAULT_META_DESCRIPTION;};?>" />
    <?php if (!empty($seo['og:image'])) {?>
    <meta name="og:image" content="<?php echo $seo['og:image'];?>"/>
    <?php } else {?>
    <meta name="og:image" content="<?php echo ROOT_URL_BASE;?>images/logo.jpg"/><?php
    }?>

    <?php echo !empty($ogTitle) ? '<meta property="og:title" content="'.$ogTitle.'" />' : '';?>
    <meta property="og:site_name" content="<?php echo defined('DEFAULT_META_TITLE') ? DEFAULT_META_TITLE : SITE_NAME; ?>"/>
    <?php echo !empty($ogImage) ? '<meta property="og:image" content="'.$ogImage.'" />' : '';?>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo ROOT_URL_BASE;?>favicon.ico">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/global.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/nav.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/colorbox/colorbox.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE;?>css/dev.css" />

    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/bootstrap.js"></script>
    <script src="<?php echo ROOT_URL_BASE;?>js/jquery.colorbox.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/helper.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/common.js"></script>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?3nq8OcgrhGKn4jANkwpumjzXSYfR9PKm';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->


</head>
<body class="<?php echo (!empty($homePage) && $homePage == true) ? 'body-img' : 'bg_pages';?>">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<input type="hidden" id="rootUrlLink" value="<?php echo ROOT_URL;?>" />
<nav role="navigation" class="navbar navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo ROOT_URL;?>" class="navbar-brand"><img src="<?php echo ROOT_URL_BASE;?>images/logo.png"></a>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php if (!empty($top_pages) && is_array($top_pages)){
                    $top_pages = sort_page_navigation($top_pages);
                    foreach($top_pages as $page_array) {
                        $page = $page_array['parent'];
                        $sub_page = !empty($page_array['children']) ? $page_array['children'] : '';
                        if ($page->id == CMS_HOME_PAGE_ID){
                            $page_class = (empty($current_slug)) ? 'active' : '';?>
                            <li class="<?php echo $page_class;?>">
                                <a href="<?php echo ROOT_URL; ?>"><?php echo $page->title; ?></a>
                            </li><?php
                        } else if (empty($sub_page)){
                            $page_url = ROOT_URL.$page->cms_slug;
                            $page_class = (!empty($current_slug) && $current_slug == $page->cms_slug) ? 'active' : '';?>
                            <li class="<?php echo $page_class;?>"><a href="<?php echo $page_url; ?>"><?php echo $page->title; ?></a></li>
                            <?php
                        } else if (!empty($sub_page)){
                            $sub_menu_html = '';
                            $page_class = '';
                            echo $page_url = '';
                            foreach ($sub_page as $subpg){
                                $sub_page_url = ROOT_URL.$subpg->cms_slug;
                                $sub_class = (!empty($current_slug) && $current_slug == $subpg->cms_slug) ? 'active' : '';
                                $page_class = (!empty($sub_class)) ? $sub_class : $page_class;
                                $sub_menu_html .= '<li class="'.$sub_class.'"><a href="'.$sub_page_url.'">'.$subpg->title.'</a></li>';
                                $page_url = empty($page_url) ? $sub_page_url : $page_url;
                            }?>
                            <li class="dropdown <?php echo $page_class; ?>">
                                <a href="<?php echo $page_url;?>" data-toggle="dropdown" class="dropdown-toggle"><?php echo $page->title; ?></a>
                                <?php if (!empty($sub_page) && is_array($sub_page)){?>
                                <ul class="dropdown-menu">
                                    <?php echo $sub_menu_html;?>
                                </ul>
                                <?php }?>
                            </li>
                            <?php
                        }
                    }
                }?>
            </ul>
        </div>
    </div>
</nav>
<?php return;?>






<div class="navbar-wrapper">
    <div class="container">
        <div class="col-md-6  logo-brand">
            <a href="<?php echo ROOT_URL;?>" class="visible-lg"><img src="<?php echo ROOT_URL_BASE;?>images/logo.jpg"></a>
            <a href="<?php echo ROOT_URL;?>" class="visible-md visible-sm visible-xs" ><img src="<?php echo ROOT_URL_BASE;?>images/logo.jpg" class="img-responsive"></a>
        </div>

        <div class="col-md-6 left-peding-none">

                <?php if( !empty($isMemberLogin) &&  $isMemberLogin != false) {?>
                    <div class="top-right-btn top-right-btn-widht-auto">
                        <ul class="nav navbar-nav login-btn-header-two">
                            <li class="dropdown">
                                <a href="<?php echo ROOT_URL;?>submit_file" class="upload_button button shadow-radial">Upload</a>
                                <a href="#" class="dropdown-toggle login button shadow-radial title-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo !empty($isMemberLogin->first_name) ? $isMemberLogin->first_name : '';?></a>

                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo MEMBER_ROOT_URL;?>my_profile">My Profile</a></li>
                                    <li><a href="<?php echo MEMBER_ROOT_URL;?>my_files">My  Files</a></li>
                                    <!--<li><a href="#">My Searches</a></li>-->
                                    <!--<li><a href="<?php /*echo MEMBER_ROOT_URL;*/?>account_settings">Account Settings</a></li>-->
                                    <li><a href="<?php echo ROOT_URL;?>logout">Sign Out</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                <?php } else {?>
                    <div class="top-right-btn">
                        <a href="<?php echo ROOT_URL;?>register_via_linkedin" class="register button shadow-radial">Register</a>
                        <ul class="nav navbar-nav login-btn-header">
                            <li class="dropdown">
                                <a href="<?php echo ROOT_URL;?>login" class="login button shadow-radial" role="button" >Login</a>
                            </li>
                        </ul>
                    </div>
                <?php }?>


            <nav class="navbar navbar-inverse navbar-static-top z-index-0">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div id="navbar" class="navbar-collapse collapse">


                    <ul class="nav navbar-nav">
                        <?php if (!empty($top_pages) && is_array($top_pages)){
                            foreach($top_pages as $page) {
//print_r($page);
                                //is_popup
                                //ajax-popup
                                if ($page->id == CMS_HOME_PAGE_ID){?>
                                    <li><a class="<?php echo empty($current_slug) ? 'active' : '';?>"
                                        href="<?php echo ROOT_URL; ?>"><?php echo $page->title; ?></a>
                                    </li><?php
                                } else {
                                    $is_popup = '';
                                    $page_url = ROOT_URL.$page->cms_slug;
                                    if (!empty($page->is_popup) && $page->is_popup == 1) {
                                        $is_popup = 'ajax-popup';
                                        $page_url = ROOT_URL.'cms/'.$page->cms_slug.'/ajax';
                                    }   ?>
                                    <li><a class="<?php echo (!empty($current_slug) && $current_slug == $page->cms_slug) ? 'active' : '';  echo ' '.$is_popup;?>"
                                        href="<?php echo $page_url; ?>"><?php echo $page->title; ?></a>
                                    </li><?php
                                }
                            }
                        }?>
                    </ul>
                </div>

            </nav>
        </div>

        <div class="col-md-12 spreter-red"></div>

    </div>
</div>


<div class="container marketing-box">