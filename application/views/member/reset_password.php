<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/1/2015
 * Time: 11:44 AM
 */?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
<header class="bp-banner discover-bnr parallax-top-bnr">

    <div class="wrapper">

        <div class="bnr-content">
            <h1><?php echo $cmsData->title;?></h1>
            <p><?php echo $cmsData->small_description;?></p>

        </div>

    </div>
</header>

<?php
$cms_wrapper = 'discover-cheating';?>
<section class="<?php echo $cms_wrapper;?>">
    <div class="container user_info_form">
        <div class="col-lg-10 peding-left-none">
        <?php
        echo validation_errors();
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
        <?php
        if (!empty($validToken)) {
            $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
            echo form_open(MEMBER_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">New Password*</label>
                    <input type="password" required="required" name="password" id="password" placeholder="Enter New Password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password*</label>
                    <input type="password" required="required" name="retype_password" id="retype_password" placeholder="Password Again" class="form-control">
                </div>
                <button class="sign-in">Change Password</button>

            </form><?php
        } else {?>
            <a href="<?php echo ROOT_URL?>forgot_password"><button class="sign-in">Forget Password</button></a><?php
        }?>
        <div class="clearfix"></div>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
        </div>
    </div>
</section>
<?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
<?php }?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
    $(function(){
        if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
            $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
        }
    });
</script>