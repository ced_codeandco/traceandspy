<div class="row">
    <div class="col-lg-12">
        <ul class="bradcram">
            <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
            <li>Upload file</li>
        </ul>
        <h3 class="text-left">Upload File</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">




        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom package_details_wrap">
                    <?php if (!empty($package_limits)) {//print_r($package_limits);?>
                    <?php if (!empty($active_package_details->title)) {?>
                    <span class="package_details_box">
                    Your active package:
                        <span class="red-text"><?php
                        echo $active_package_details->title;?>
                        </span>
                        <?php if (!empty($package_limits) && $package_limits->price == 0) {?>

                        | <a class="margin_left red-text" href="<?php echo ROOT_URL;?>select_package/<?php echo $user_id;?>">Upgrade Package</a>

                        <?php }?>
                    </span>
                    <?php }?>
                    <span class="package_details_box">
                    Total Number of files pending in your package:
                        <span class="red-text"><?php
                        echo $pending_files_package = !empty($package_limits->pending_files_in_package) ? $package_limits->pending_files_in_package : 0;?>
                        </span>
                    </span>
                    <span class="package_details_box">
                    Number of files pending in your package today:
                        <span class="red-text"><?php
                            echo $files_pending_today = !empty($package_limits->pending_files_in_package) ? $package_limits->pending_files_in_package : 0;
                        ?>
                        </span>
                    </span>
                    <span class="package_details_box">
                        Your package valid til:
                        <span class="red-text"><?php
                            echo (!empty($package_limits->current_package_end_date) && $package_limits->current_package_end_date != '0000-00-00 00:00:00') ? date('d F Y', strtotime($package_limits->current_package_end_date)) : 0;?>
                        </span>
                    </span>
                    <span class="package_details_box">
                    Allowed file type: <span class="red-text"><?php echo implode(', ', explode('|', strtoupper(ALLOWED_FILE_TYPES)));?></span>
                    </span>
                    <?php } else if (empty($package_limits)){?>

                    You are not subscribed to any packages yet. Click on "Browse Packages" to select one

                    <?php } ?>

                </p>
                <div class="col-lg-8 peding-left-none">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <?php if (!empty($package_limits->pending_file_count) && $package_limits->pending_file_count > 0 && !empty($files_pending_today) && $files_pending_today > 0) {?>
                    <form class="register-form profile-lable" name="createClassified" method="post" id="createClassified" enctype="multipart/form-data">
                        <input type="hidden" name="created_by" value="<?php echo $user_id;?>" />


                        <div class="form-group">
                            <div id="fileupload_message"></div>
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Upload file</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none ">
                                <span class="fileupload_control_wrap">
                                    <label class="file_upload_label">Add Files</label>
                                    <input id="fileupload_control" type="file" name="files[]" data-url="upload_file" multiple="multiple" style="cursor: pointer;">
                                </span>
                                <input type="hidden" id="max_file_count" value="<?php echo !empty($files_pending_today) ? $files_pending_today : 0;?>">
                                <div class="file_previw_wrap" id="file_previw_wrap_id">
                                    <ul>

                                    </ul>
                                </div>
                            </div>

                        </div>



                        <div class="clearfix"></div>
                        <button type="submit" class="update-profile-btn">Submit Files</button>


                            </form>
                    <?php } else if (!empty($package_limits) && $package_limits != false && (empty($files_pending_today) OR $files_pending_today < 1)){?>
                        <p>
                            Sorry! You have consumed all allowed file uploads of the day. Please try again tomorrow.
                            <?php echo (!empty($package_limits->pending_file_count) && $package_limits->pending_file_count > 0) ? "<br />You have $package_limits->pending_file_count files pending in your package." : '';?><br />
                        </p>
                    <?php } else {?>
                        <a href="<?php echo ROOT_URL;?>select_package/<?php echo $user_id;?>"><button class="update-profile-btn">Browse Packages</button></a>
                    <?php } ?>
                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/jquery.fileupload.js"></script>
<script>
    $(function () {
        var maxNumberOfFiles = parseInt($('#max_file_count').val());
        $('#fileupload_control').fileupload({
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(pdf)$/i,
            maxNumberOfFiles: maxNumberOfFiles,
            done: function (e, data) {
                var result = data.result;
                //console.log(result);
                if (result.status == 1) {
                    if ($('#file_previw_wrap_id ul li').length < maxNumberOfFiles) {
                        $('<li/>').text(result.original_file_name)
                            .append('<input type="hidden" name="uploaded_files[]" value="' + result.uploaded_name + '" />')
                            .append('<input type="hidden" name="uploaded_title[]" value="' + result.original_file_name + '" />')
                            .append('<span class="delete_btn" onclick="delete_uploaded_file(this, \'' + result.uploaded_name + '\')">Delete</span>')
                            .append('<span><img class="hidden" src="images/ajax_load.gif" /><span>')
                            .appendTo($('#file_previw_wrap_id ul'));
                        $('#fileupload_control-error').hide();
                        if ($('#file_previw_wrap_id ul li').length >= maxNumberOfFiles) {
                            $('.fileupload_control_wrap').hide();
                            $('#fileupload_message').html('Maximum number of files: '+maxNumberOfFiles).show();
                        }
                        //$('#file_previw_wrap_id ul').append('<li>'+result.original_file_name+'</li>');
                    } else {
                        //alert(maxNumberOfFiles);
                        $('.fileupload_control_wrap').hide();
                        $('#fileupload_message').html('Maximum number of files: '+maxNumberOfFiles).show();
                    }
                } else {
                    alert(result.message);
                }
                /*$.each(data.result.files, function (index, file) {

                    $('<p/>').text(file.name).appendTo(document.body);
                });*/
            }
        });
    });
    function delete_uploaded_file(obj, file_name) {
        var maxNumberOfFiles = parseInt($('#max_file_count').val());
        var url_base = $('#rootUrlLink').val();
        var delete_url = url_base+'upload_file?action=delete&file='+file_name;
        $.getJSON(delete_url, function(data){
            if (data.status == 1) {
                $(obj).parent().remove();
                //alert($('#file_previw_wrap_id ul li').length + ' - ' +maxNumberOfFiles);
                if ($('#file_previw_wrap_id ul li').length < maxNumberOfFiles) {
                    $('.fileupload_control_wrap').show();
                    $('#fileupload_message').hide();
                }
            }
            //console.log(data);
        })
        return false;
    }
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        jQuery.validator.addMethod("file_required", function(value, element) {
            //$(element).val(value.trim());
            //alert($('#file_previw_wrap_id ul li').length);
            return this.optional(element) && ($('#file_previw_wrap_id ul li').length > 0);
        }, "Please upload a file");
        $("#createClassified").validate({
            rules: {
                /*title:{
                    required: true,
                    maxlength: 200
                },*/
                'files[]':{
                    file_required: true,
                 },

            },
            messages: {
                /*title:{
                    required: 'Please enter a title',
                    maxlength: 'Maximum length allowed is 200'
                },*/
                'files[]':{
                    file_required: 'Please upload file',
                }
            }
        });
    })
    <?php
        $classifiedDetails = (array) $classifiedDetails;
        if(!empty($classifiedDetails) && is_array($classifiedDetails)) {?>
    $(document).ready(function(){
        <?php echo build_filldata_javascript($classifiedDetails);?>
    })
    <?php }?>
</script>