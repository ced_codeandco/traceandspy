<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/1/2015
 * Time: 11:44 AM
 */?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/discover-cheating.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/protect-children.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/monitor-workers.css">
<header class="bp-banner discover-bnr parallax-top-bnr">

    <div class="wrapper">

        <div class="bnr-content">
            <h1><?php echo $cmsData->title;?></h1>
            <p><?php echo $cmsData->small_description;?></p>

        </div>

    </div>
</header>

<?php
$cms_wrapper = 'discover-cheating';?>
<section class="<?php echo $cms_wrapper;?>">
    <div class="container user_info_form">
        <div class="col-lg-10 peding-left-none">
    <?php echo validation_errors();?>
    <div class="col-lg-4">
        <?php
        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
        echo form_open(ROOT_URL.'forgot_password',$attributes);
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Email Address*</label>
            <input type="email" required="required" name="username" id="username" placeholder="username@provider.com" value="<?php echo !empty($_POST['username']) ? $_POST['username'] : ''?>" class="form-control">
        </div>


        <button type="submit" class="sign-in">Reset Password</button>
        <div class="clearfix"></div>

        </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
    </div><!-- /.col-lg-4 -->
        </div>
    </div>
</section>
<?php if (!empty($cmsData->cms_banner_image) && !is_dir(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <input type="hidden" id="parallax-image-banner-top" value="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" />
<?php }?>

<script src="<?php echo ROOT_URL_BASE;?>js/parallax.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/bp-common.js"></script>
<script type="text/javascript">
    $(function(){
        if ($('#parallax-image-banner-top').length > 0 && $('#parallax-image-banner-top').val() != '') {
            $('.parallax-top-bnr').parallax({imageSrc: $('#parallax-image-banner-top').val()});
        }
    });
</script>