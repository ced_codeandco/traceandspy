<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 12/8/2015
 * Time: 3:42 PM
 */
///var_dump($packageDetails); ?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="row">
    <div class="col-lg-12">
        <ul class="bradcram">
            <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
            <li>Register</li>
        </ul>
        <h3 class="text-left">Confirm Payment</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">




        <div id="myTabContent" class="tab-content ">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom">
                    <strong>Click on <span class="red-text">PAY NOW</span> to proceed to payment gateway.</strong>
                </p>

                <div class="col-lg-13 peding-left-none">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form method="post" action=""  name="registerForm" id="registerForm" class="profile-lable">
                        <input type="hidden" name="user_id" value="<?php echo $member_id;?>">
                        <?php if (!empty($packageDetails)) {?>
                            <table class="table_package">
                                <tr>
                                    <th class="package-table-col-1">Package</th>
                                    <th class="package-table-col-2">Benefits</th>
                                    <th class="package-table-col-3">Price USD</th>
                                    <th class="package-table-col-4">&nbsp;&nbsp;</th>
                                </tr>
                                <?php

                                $i = 0;
                                //foreach ($package_list as $package) {//classified_slug
                                $package = $packageDetails;?>
                                    <tr class="package_row">
                                        <td class="package-table-col-1"><?php echo $package->title; ?></td>
                                        <td class="package-table-col-2"><?php echo $package->description; ?></td>
                                        <td class="package-table-col-3"><?php echo $package->price; ?></td>
                                        <td class="package-table-col-4">
                                            <input type="radio" name="package_id" id="package_selector_<?php echo $package->id;?>" value="<?php echo $package->id;?>" checked="checked">
                                        </td>
                                    </tr>
                                <?php


                                //}?>
                            </table>
                            <div class="package_head">
                                <div class="tit"></div>
                            </div><?php
                        }?>

                    </form>
                    <div class="clearfix"></div>


                    <form action='<?php echo $two_co_post_url;?>' method='post' id="two_co_form">

                        <input type='hidden' name='sid' value='<?php echo $two_co_account_number;?>' />
                        <input type='hidden' name='mode' value='2CO' />
                        <?php if (!empty($two_co_account_mode) && $two_co_account_mode =='demo') {?>
                            <input type='hidden' name='demo' value='Y' />
                        <?php }?>
                        <input type='hidden' name='li_0_name' value='<?php echo $packageDetails->title;?>' />
                        <input type='hidden' name='li_0_price' value='<?php echo $packageDetails->price;?>' />
                        <input type='hidden' name='li_0_tangible' value='N' />
                        <input type='hidden' name='custom_userid' value='<?php echo $member_id;?>' />
                        <input type='hidden' name='package_id' value='<?php echo $packageDetails->id;?>' />
                        <input type='hidden' name='x_receipt_link_url' value='<?php echo ROOT_URL;?>payment_status' />
                        <input type='hidden' name='currency_code' value='USD' />
                        <button class="update-profile-btn" id="shop_at_checkout">Pay Now</button>
                    </form>


                    <div class="clearfix"></div>

                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>

