<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">Verify Email</h3>
        <p>Don't have an <strong class="red-text">P&ID</strong> account? Please <a href="<?php echo ROOT_URL?>register">register</a>. (It's FREE!)</p>
    </div>

    <div class="col-lg-7">
        <?php
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>


        <a href="<?php echo ROOT_URL?>login"><button class="sign-in">Login</button></a>
        <div class="clearfix"></div>


        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
    </div><!-- /.col-lg-4 -->

    <!-- /.col-lg-4 -->


</div>
<?php
return;/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 4/24/2015
 * Time: 2:23 AM
 */?>
<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/1/2015
 * Time: 11:44 AM
 */?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="main-wraper">

    <div class="container">

        <div class="pages">

            <ul class="breadcrumbs"><li><a href="<?php echo ROOT_URL;?>">Home</a></li><li>Forgot Password</li></ul>

            <h2 class="page_heading">Forgot Password</h2>

            <div class="pages_left">
                <?php
                echo validation_errors();
                $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
                echo form_open(MEMBER_ROOT_URL.'forgot_password',$attributes); ?>
                <div class="registration_login forget_pwd">

                    <h2>Verify Your Email Address</h2>
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }
                    ?>

                    <span class="reg_term">Don't have an account? <a href="<?php echo ROOT_URL;?>register">Register</a></span>

                </div>
                </form>
            </div>


        </div>

    </div>

</div>








