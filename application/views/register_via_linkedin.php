<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : 'submit_file');?>">
<div class="row">
    <div class="col-lg-12">
        <ul class="bradcram">
            <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
            <li>Register with linkedin</li>
        </ul>
        <h3 class="text-left">Register with linkedin</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">




        <div id="myTabContent" class="tab-content ">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom">Click on Register with Linkedin button to create a <?php echo SITE_NAME;?> account with linkedin</p>

                <div class="col-lg-9 peding-left-none">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>

                    <div class="linkedin_register_wrap">
                        <!--<p></p>-->
                        <div class="social-link"><a href="#" class="linkdine" id="linkedInBtn">Register with Linkedin</a></div>
                        <span class="register_alt_txt"><a href="<?php echo ROOT_URL?>register">Do not have linkedin account? Please register with <?php echo SITE_NAME;?></a>.</span>
                    </div>

                    <div class="clearfix"></div>


                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<input type="hidden" id="linked-in-post" value="Checkout <?php echo SITE_NAME. ' | ' .ROOT_URL;?>">
<script src="<?php echo ROOT_URL_BASE?>js/linkedInLogin.js"></script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 77rf7f8avavofz
    authorize: true
    onLoad: onLinkedInLoad
</script>


