<div class="row cms_page_wrap">
    <div class="col-lg-12">
        <h3 class="text-left">News</h3>
    </div>
    <div class="devider-25px"></div>


    <div class="col-lg-12">
        <ul class="slides news_list_page"><?php
            if (!empty($news_list) && is_array($news_list)) {
                foreach ($news_list as $news) { ?>
                    <li>
                        <a class="news_title" href="<?php echo ROOT_URL.'news/'.$news->url_slug;?>"><?php echo $news->title;?>:</a>
                        <?php echo stripslashes($news->small_description);?>
                        <a class="read_more" href="<?php echo ROOT_URL.'news/'.$news->url_slug;?>">Read more &raquo;</a>
                    </li>
                <?php
                }
            }?>
        </ul>
        <?php echo $pagination;?>
    </div>

</div>
