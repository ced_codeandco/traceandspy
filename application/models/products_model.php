<?php
class Products_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		
		$order = $this->getLastOrder();
		$product_order = $order + 1;
		
		$data = array(
			'title' => $this->input->post('title'),
			'product_slug' => $this->input->post('product_slug'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'home_title' => $this->input->post('home_title'),
			'home_sub_title' => $this->input->post('home_sub_title'),
			'home_description' => $this->input->post('home_description'),
			'description' => $this->input->post('description'),
			'product_banner_image' => $this->input->post('product_banner_image'),
			'additional_image_1' => $this->input->post('additional_image_1'),
			'additional_image_2' => $this->input->post('additional_image_2'),
			'additional_image_3' => $this->input->post('additional_image_3'),
			'price' => $this->input->post('price'),
			'bullet_description' => $this->input->post('bullet_description'),
			'footer_text' => $this->input->post('footer_text'),
			'product_order' => $product_order,
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
			'is_popup' => $this->input->post('is_popup'),
			'is_active' => $this->input->post('is_active'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
			'created_date_time' =>date('Y-m-d H:i:s'),
            'created_ip' => $this->input->ip_address()
		);



		$this->db->insert('tbl_product',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){

		$data = array(
			'title' => $this->input->post('title'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'home_title' => $this->input->post('home_title'),
			'home_sub_title' => $this->input->post('home_sub_title'),
			'home_description' => $this->input->post('home_description'),
			'description' => $this->input->post('description'),
			'product_banner_image' => $this->input->post('product_banner_image'),
            'additional_image_1' => $this->input->post('additional_image_1'),
            'additional_image_2' => $this->input->post('additional_image_2'),
            'additional_image_3' => $this->input->post('additional_image_3'),
            'price' => $this->input->post('price'),
            'bullet_description' => $this->input->post('bullet_description'),
            'footer_text' => $this->input->post('footer_text'),
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
            'is_popup' => $this->input->post('is_popup'),
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id'),
            'updated_ip' => $this->input->ip_address()
		);

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_product',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_product SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_product SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_product') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
    function generateCleanSlug($string, $id=0){
        //Set char set to UTF-8 - to manage accented characters
        setlocale(LC_ALL, "en_US.UTF8");
        //Remove everything except alphabets and digits
        $url_string=preg_replace("/[^a-z0-9]/i"," ", ltrim(rtrim(strtolower($string))));
        //Remove multiple spaces
        $url_string = preg_replace("/\s+/", " ", $url_string);
        //Replace space with dashes
        $clean_url = str_replace(" ","-",$url_string);
        $newurl_string = $clean_url;
        //Condition for add / update
        if(!empty($id) && $id!=0){
            $condition = "id!='".$id."' AND ";
        }else{
            $condition ="";
        }
        $queryCount = 'SELECT product_slug from tbl_product WHERE '.$condition.'
                    product_slug LIKE "'.$newurl_string.'"';

        //Check duplicate
        $rqC = mysql_num_rows(mysql_query($queryCount));
        $i=0;
        while($rqC != 0) {
            $i++;
            //Add number to avoid duplicate
            $newurl_string = $clean_url."-".$i;
            $queryCount = 'SELECT product_slug from tbl_product WHERE '.$condition.'
                    product_slug LIKE "'.$newurl_string.'"';
            $rqC = mysql_num_rows(mysql_query($queryCount));
        }

        return $newurl_string;
    }
	function generateProductSlug($title='product page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT product_slug from tbl_product WHERE product_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_product WHERE 1=1 ";

		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				$query_data[$i]->sub_page_count = $this->getSubPageCount($value['id']);
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getProductPageList($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_product WHERE 1=1 ";


		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				if (!empty($value['id'])) {
                    $sql1 = "select $all FROM tbl_product WHERE parent_id = " . $value['id'] . " AND is_deleted='0' AND is_active='1'  ";
                    $query = $this->db->query($sql1);
                    $query_data[$i]->sub_page_List = $query->result();
                }
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getSubPageCount($parent_id){

		$sql ="select count(id) as sub_page_count FROM tbl_product WHERE parent_id = $parent_id AND is_deleted='0'   ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select product_order FROM tbl_product ORDER BY product_order desc LIMIT 0,1 ";
		$query = $this->db->query($sql);
		$result = $query->result();

		return !empty($result[0]->product_order) ? $result[0]->product_order : 0;
	}
	
	function changeOrder($id,$product_order,$position){
		if($id!='' && $product_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);

			if($position=='Dn'){
				$qr="select product_order,id from tbl_product where parent_id = $pageDetails->parent_id  AND product_order > '".$product_order."' AND is_deleted='0' order by product_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->product_order;
					$qry = "UPDATE tbl_product SET `product_order`= $product_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_product SET `product_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select product_order,id from tbl_product where parent_id = $pageDetails->parent_id  AND product_order < '".$product_order."' AND is_deleted='0' order by product_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
                //print_r(array($id,$product_order,$position));
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->product_order;
					$qry = "UPDATE tbl_product SET `product_order`= $product_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_product SET `product_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
		}
	}

    /**
     * Function to get page details with slug
     *
     * @param $product_slug
     *
     * @return bool
     */
    function getPageDetails($product_slug){
        $this->db->where('product_slug', $product_slug);
        $query = $this->db->get('tbl_product');
        $query_data = $query->result();

        return ($query_data != false && is_array($query_data)) ? $query_data[0] : false;
    }

    function get_product_navigation()
    {
        $pages_list = $this->getAllRecords('id, title, product_slug, is_popup, parent_id, product_order', " on_header ='1' AND is_active = '1' ", 'ORDER BY product_order ASC');
        $page_lookup = array();
        if (!empty($pages_list) && is_array($pages_list)) {
            foreach ($pages_list as $page) {
                if (empty($page->parent_id)) {
                    $page_lookup[$page->id]['parent'] = $page;
                } else if (!empty($page->parent_id)) {
                    $page_lookup[$page->parent_id]['children'][] = $page;
                }
            }
        }

        return $page_lookup;
    }

    function populateSeoTags($itemDetails, $post = null)
    {
        $itemDetails = (array) $itemDetails;
        $post = (array) $post;
        $seoTags = '';
        if (!empty($itemDetails['meta_title'])) {
            $seoTags['title'] = $itemDetails['meta_title'];
        } else if (!empty($post['meta_title'])) {
            $seoTags['title'] = strip_tags($post['meta_title']);
        } else if (!empty($itemDetails['title'])) {
            $seoTags['title'] = $itemDetails['title'];
        } else if (!empty($post['title'])) {
            $seoTags['title'] = strip_tags($post['title']);
        }

        if (!empty($itemDetails['meta_keywords'])) {
            $seoTags['tags'] = $itemDetails['meta_keywords'];
        } else if (!empty($post['meta_keywords'])) {
            $seoTags['tags'] = strip_tags($post['meta_keywords']);
        }

        if (!empty($itemDetails['meta_desc'])) {
            $seoTags['description'] = strip_tags($itemDetails['meta_desc']);
        } else if (!empty($post['meta_desc'])) {
            $seoTags['description'] = strip_tags($post['meta_desc']);
        } elseif (!empty($itemDetails['description'])) {
            $seoTags['description'] = strip_tags($itemDetails['description']);
        } else if (!empty($post['description'])) {
            $seoTags['description'] = strip_tags($post['description']);
        }

        if (!empty($itemDetails['title'])) {
            $seoTags['og:title'] = !empty($post['title']) ? $post['title'] : $seoTags['title'];
        }

        return $seoTags;
    }

    function getProductsForList($all='*',$where='',$orderby='',$limit='', $getTotalRecords = false){

        $sql ="select ".( ($getTotalRecords == true) ? 'SQL_CALC_FOUND_ROWS' : '' )." $all FROM tbl_product WHERE 1=1 ";
        if($where!=''){
            $sql .= " AND $where ";
        }
        $sql .= " AND is_deleted='0' ";


        if($orderby!=''){
            $sql .= " $orderby ";
        }
        if($limit!=''){
            $sql .= " $limit ";
        }
        //echo "<br />\n\n".$sql."\n\n";
        $query = $this->db->query($sql);
        if ($getTotalRecords == true) {
            $rc = $this->db->query("SELECT FOUND_ROWS()");
            $row = $rc->row_array();
            $total_row_count = $row['FOUND_ROWS()'];
        }
        $query_data = $query->result();
        if ($getTotalRecords == true) {

            return array(
                'total_row_count' => $total_row_count,
                'result' => $query_data,
            );
        }
        return $query_data;
    }

    /**
     * Function to build pagination links
     *
     * @param $searchResultTotal
     * @param $per_page
     * @param $searchCriteria
     *
     * @return string
     */
    function build_paginator($searchResultTotal, $per_page, $searchCriteria = '', $base_url = '')
    {
        $this->load->library('pagination');

        if (isset($searchCriteria['per_page'])) unset($searchCriteria['per_page']);
        //$config['uri_segment'] = 2;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['base_url'] = !empty($base_url) ? $base_url : ROOT_URL.CMS_PRODUCT_PAGE_URL.'?';
        $config['total_rows'] = $searchResultTotal;
        $config['per_page'] = $per_page;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<span aria-hidden="true">&lang;&lang;</span>';;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<span aria-hidden="true">&lang;</span>';
        $config['prev_tag_open'] = '<li class="prev_li" >';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<span aria-hidden="true">&rang;</span>';
        $config['next_tag_open'] = '<li class="next_li">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active_li"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';

        $config['last_link'] = '<span aria-hidden="true">&rang;&rang;</span>';;
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    function add_member_purchase($data) {
        //if (!empty($data['order_number']))
        $this->db->insert('tbl_payment_info', $data) or die(mysql_error());
        $id = mysql_insert_id();
        //echo $this->db->last_query();

        return $id;
    }

    function getPaymentDetails($payment_id) {
        $this->db->where('id', $payment_id);
        $query = $this->db->get('tbl_payment_info') or die(mysql_error());
        if($query->num_rows >= 1)
            return $query->row();
        else
            return false;
    }
}