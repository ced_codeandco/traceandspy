<?php
class Cms_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		
		$order = $this->getLastOrder();
		$cms_order = $order + 1;
		
		$data = array(
			'title' => $this->input->post('title'),
			'cms_slug' => $this->input->post('cms_slug'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'home_title' => $this->input->post('home_title'),
			'home_sub_title' => $this->input->post('home_sub_title'),
			'home_description' => $this->input->post('home_description'),
			'description' => $this->input->post('description'),
			'cms_banner_image' => $this->input->post('cms_banner_image'),
			'additional_image_1' => $this->input->post('additional_image_1'),
			'additional_image_2' => $this->input->post('additional_image_2'),
			'additional_image_3' => $this->input->post('additional_image_3'),
			'footer_text' => $this->input->post('footer_text'),
			'map_info' => $this->input->post('map_info'),
			'cms_order' => $cms_order,
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
			'is_popup' => $this->input->post('is_popup'),
			'is_active' => $this->input->post('is_active'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
			'created_date_time' =>date('Y-m-d H:i:s'),
            'created_ip' => $this->input->ip_address()
		);



		$this->db->insert('tbl_cms',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		if(LOG_ENTRY == TRUE){
			$id = $this->addCMSLog($this->input->post('id'));
		}
		$data = array(
			'title' => $this->input->post('title'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'home_title' => $this->input->post('home_title'),
			'home_sub_title' => $this->input->post('home_sub_title'),
			'home_description' => $this->input->post('home_description'),
			'description' => $this->input->post('description'),
			'cms_banner_image' => $this->input->post('cms_banner_image'),
            'additional_image_1' => $this->input->post('additional_image_1'),
            'additional_image_2' => $this->input->post('additional_image_2'),
            'additional_image_3' => $this->input->post('additional_image_3'),
            'footer_text' => $this->input->post('footer_text'),
            'map_info' => $this->input->post('map_info'),
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
            'is_popup' => $this->input->post('is_popup'),
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id'),
            'updated_ip' => $this->input->ip_address()
		);

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_cms',$data);
		
		
		return true;
		
	}
	function addCMSLog($id){
		//$cmsDetails = $this->getDetails($id);

        $this->db->where('is_deleted', '0');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_cms') or die(mysql_error());

        if($query->num_rows >= 0) {
            $new_author = $query->result_array();

            foreach ($new_author as $row => $author) {
                $this->db->insert("tbl_cms_log", $author);
            }
        }
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_cms SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_cms SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_cms') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateCMSSlug($title='cms page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT cms_slug from tbl_cms WHERE cms_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_cms WHERE 1=1 ";

		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				$query_data[$i]->sub_page_count = $this->getSubPageCount($value['id']);
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getCMSPageList($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_cms WHERE 1=1 ";


		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				if (!empty($value['id'])) {
                    $sql1 = "select $all FROM tbl_cms WHERE parent_id = " . $value['id'] . " AND is_deleted='0' AND is_active='1'  ";
                    $query = $this->db->query($sql1);
                    $query_data[$i]->sub_page_List = $query->result();
                }
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getSubPageCount($parent_id){

		$sql ="select count(id) as sub_page_count FROM tbl_cms WHERE parent_id = $parent_id AND is_deleted='0'   ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select cms_order FROM tbl_cms ORDER BY cms_order desc LIMIT 0,1 "; 
		$query = $this->db->query($sql);
		$result = $query->result();

		return $result[0]->cms_order;
	}
	
	function changeOrder($id,$cms_order,$position){
		if($id!='' && $cms_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);

			if($position=='Dn'){
				$qr="select cms_order,id from tbl_cms where parent_id = $pageDetails->parent_id  AND cms_order > '".$cms_order."' AND is_deleted='0' order by cms_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->cms_order;
					$qry = "UPDATE tbl_cms SET `cms_order`= $cms_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_cms SET `cms_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select cms_order,id from tbl_cms where parent_id = $pageDetails->parent_id  AND cms_order < '".$cms_order."' AND is_deleted='0' order by cms_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
                //print_r(array($id,$cms_order,$position));
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->cms_order;
					$qry = "UPDATE tbl_cms SET `cms_order`= $cms_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_cms SET `cms_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
		}
	}

    /**
     * Function to get page details with slug
     *
     * @param $cms_slug
     *
     * @return bool
     */
    function getPageDetails($cms_slug){
        $this->db->where('cms_slug', $cms_slug);
        $query = $this->db->get('tbl_cms');
        $query_data = $query->result();

        return ($query_data != false && is_array($query_data)) ? $query_data[0] : false;
    }

    function get_cms_navigation()
    {
        $pages_list = $this->getAllRecords('id, title, cms_slug, is_popup, parent_id, cms_order', " on_header ='1' AND is_active = '1' ", 'ORDER BY cms_order ASC');
        $page_lookup = array();
        if (!empty($pages_list) && is_array($pages_list)) {
            foreach ($pages_list as $page) {
                if (empty($page->parent_id)) {
                    $page_lookup[$page->id]['parent'] = $page;
                } else if (!empty($page->parent_id)) {
                    $page_lookup[$page->parent_id]['children'][] = $page;
                }
            }
        }

        return $page_lookup;
    }

    function populateSeoTags($itemDetails, $post = null)
    {
        $itemDetails = (array) $itemDetails;
        $post = (array) $post;
        $seoTags = '';
        if (!empty($itemDetails['meta_title'])) {
            $seoTags['title'] = $itemDetails['meta_title'];
        } else if (!empty($post['meta_title'])) {
            $seoTags['title'] = strip_tags($post['meta_title']);
        } else if (!empty($itemDetails['title'])) {
            $seoTags['title'] = $itemDetails['title'];
        } else if (!empty($post['title'])) {
            $seoTags['title'] = strip_tags($post['title']);
        }

        if (!empty($itemDetails['meta_keywords'])) {
            $seoTags['tags'] = $itemDetails['meta_keywords'];
        } else if (!empty($post['meta_keywords'])) {
            $seoTags['tags'] = strip_tags($post['meta_keywords']);
        }

        if (!empty($itemDetails['meta_desc'])) {
            $seoTags['description'] = strip_tags($itemDetails['meta_desc']);
        } else if (!empty($post['meta_desc'])) {
            $seoTags['description'] = strip_tags($post['meta_desc']);
        } elseif (!empty($itemDetails['description'])) {
            $seoTags['description'] = strip_tags($itemDetails['description']);
        } else if (!empty($post['description'])) {
            $seoTags['description'] = strip_tags($post['description']);
        }

        if (!empty($itemDetails['title'])) {
            $seoTags['og:title'] = !empty($post['title']) ? $post['title'] : $seoTags['title'];
        }

        return $seoTags;
    }
}