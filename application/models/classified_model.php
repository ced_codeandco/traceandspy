<?php
class Classified_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';
    public $searchRegions    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($data = array()){
        if (empty($data) OR !is_array($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'file_name' => $this->input->post('file_name'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->input->post('created_by'),
                'classified_ip' => $this->input->ip_address(),
                'created_date_time' => date('Y-m-d H:i:s')
            );
        }

        $userEmail = !empty($this->headerData['isMemberLogin']->email) ? $this->headerData['isMemberLogin']->email : '';
        //$this->update_log_file($userEmail, FILE_STATUS_UPLOADED);

		$this->db->insert('tbl_files',$data) or die(mysql_error());

		$classified_id = mysql_insert_id();

        return $classified_id;
	}


    function deleteRecord($id, $user_id = ''){

        mysql_query("UPDATE tbl_files SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."', updated_ip = '".$this->input->ip_address()."' WHERE id= ".$id."");

        //$this->deletePrcessStatus();
        return true;
    }

    function update_log_file($userEmail, $status = 1) {
        $emailLogs = $this->readLogFile(FILE_PATH_CONVERSION_EMAIL);

        if (in_array($userEmail, $emailLogs)) {
            $key = array_search($userEmail, $emailLogs);

            $folderLogs = $this->readLogFile(FILE_PATH_CONVERSION_FOLDER);
            $folderLogs[$key] = DIR_UPLOAD_FILE.$userEmail;
            $this->updateLogsFromArray(FILE_PATH_CONVERSION_FOLDER, $folderLogs);

            $statusLogs = $this->readLogFile(FILE_PATH_CONVERSION_STATUS);
            $statusLogs[$key] = $status;
            $this->updateLogsFromArray(FILE_PATH_CONVERSION_STATUS, $statusLogs);
        } else {
            //echo "\t Here "; print_r($emailLogs);

            $emailLogs[] = $userEmail;
            $this->updateLogsFromArray(FILE_PATH_CONVERSION_EMAIL, $emailLogs);

            $folderLogs = $this->readLogFile(FILE_PATH_CONVERSION_FOLDER);
            $folderLogs[] = trim(DIR_UPLOAD_FILE.$userEmail);
            $this->updateLogsFromArray(FILE_PATH_CONVERSION_FOLDER, $folderLogs);

            $statusLogs = $this->readLogFile(FILE_PATH_CONVERSION_STATUS);
            $statusLogs[] = $status;
            $this->updateLogsFromArray(FILE_PATH_CONVERSION_STATUS, $statusLogs);
        }
    }

    function readLogFile($filePath){
        $fileContentArray = array();
        if (file_exists($filePath)) {
            $fileContent = file_get_contents($filePath);
            //$fileContent =
            if (!empty($fileContent)) {
                $delimiter = "\t";
                $fileContentArray = str_getcsv($fileContent, $delimiter);
                $fileContentArray = array_map('trim', $fileContentArray);
            }
        }

        return $fileContentArray;
    }

    function updateLogsFromArray($path, $logArray) {
        $fp = fopen($path, 'w');
        fputcsv($fp, $logArray, "\t", chr(0));
    }

	function getCount($status=''){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
		if($status == ''){
			$sql ="select count(id) as total_classified FROM tbl_files WHERE  1=1 ";
		}else{
			$sql ="select count(id) as total_classified FROM tbl_files WHERE is_active = '$status'";
		}
        $sql .= " AND language_id='$language_id' ";
		$query = $this->db->query($sql);
		$result = $query->result();
		
		return $result[0]->total_classified;
		
	}
	function updateDetails($classifiedId = false, $data = array()){

		if(LOG_ENTRY == TRUE){
			//$id = $this->addCMSLog($classifiedId);
		}

        if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'file_name' => $this->input->post('file_name'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->input->post('created_by'),
                'classified_ip' => $this->input->ip_address(),
            );
        }
        if (empty($data['updated_ip'])) {
            $data['updated_ip'] = $this->input->ip_address();
        }


        $this->db->where("id", empty($classifiedId) ? $this->input->post('id') : $classifiedId);
		$this->db->update('tbl_files',$data);

		return true;
	}

	function getSearchResult($all='*', $searchString, $records_per_page = 10, $starts_with, $order_by, $order_by_modifier){
		$query = "SELECT $all FROM tbl_files
        WHERE is_deleted = '0' AND is_active = '1'$searchString";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $query .= " AND language_id='$language_id' ";

        $query .= !empty($order_by) ? " ORDER BY $order_by" : '';
        $query .= (!empty($order_by) && !empty($order_by_modifier)) ? " $order_by_modifier" : '';
        $query .= " LIMIT $starts_with, $records_per_page";

		$query = $this->db->query($query);
        //echo $this->db->last_query();
		$result = $query->result();
		if(count($result) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				$result[$i]->image_count = $this->getTotalImageCount($value['id']);
				$result[$i]->image_list = $this->getTotalImage($value['id']);
				$result[$i]->city = $this->getCityById($value['classified_city']);
				$result[$i]->country = $this->getCountryById($value['classified_country']);
				$result[$i]->locality = $this->getLocalityById($value['classified_locality']);
				$i++;
			}
		}
		return $result;
	}
	function getSearchResultTotal($searchString){
		$queryCount = "SELECT count(id) as total_rec FROM tbl_files WHERE is_deleted = '0' AND is_active = '1'$searchString";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $queryCount .= " AND language_id='$language_id' ";
		$query = $this->db->query($queryCount);
		$result = $query->result();
		$totalRecords = $result[0]->total_rec;

		return $totalRecords;
	}
	function addCMSLog($id){
		$classifiedDetails = $this->getDetails($id);

		$data = array(
			'title' => $classifiedDetails->title,
			'classified_id' =>  $classifiedDetails->id,
			'classified_slug' =>  $classifiedDetails->classified_slug,
			'classified_tags' =>  $classifiedDetails->classified_tags,
			'category_id' =>  $classifiedDetails->category_id,
			'brand_id' => $this->input->post('brand_id'),
			'model_id' => $this->input->post('model_id'),
			'manufacture_year' => $this->input->post('manufacture_year'),
			'small_description' => $classifiedDetails->small_description,
			'description' =>  $classifiedDetails->description,
			'amount' => $this->input->post('amount'),
			'classified_contact_no' =>  $classifiedDetails->classified_contact_no,
			'meta_title' => $classifiedDetails->meta_title,
			'meta_desc' => $classifiedDetails->meta_desc,
			'meta_keywords' => $classifiedDetails->meta_keywords,
			'classified_contact_name' => $classifiedDetails->classified_contact_name,
			'classified_contact_email' => $classifiedDetails->classified_contact_email,
			'classified_address' => $classifiedDetails->classified_address,
			'classified_land_mark' => $classifiedDetails->classified_land_mark,
			'classified_country' => $classifiedDetails->classified_country,
			'classified_city' => $classifiedDetails->classified_city,
			'classified_ip' => $classifiedDetails->classified_ip,
			'created_by' => $classifiedDetails->created_by,
			'updated_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_files_log',$data) or die(mysql_error()); 	
		return mysql_insert_id();
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_files SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_files') or die(mysql_error());
        //echo $this->db->last_query();
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsBySlug($classified_slug){
		$this->db->where('is_deleted', '0');
		$this->db->where('classified_slug', $classified_slug);
		$query = $this->db->get('tbl_files') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateClassifiedSlug($title='classified'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT classified_slug from tbl_files WHERE classified_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit='', $getTotalRecords = false){
		
		$sql ="select ".( ($getTotalRecords == true) ? 'SQL_CALC_FOUND_ROWS' : '' )." $all FROM tbl_files WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";


		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
        //echo "<br />\n\n".$sql."\n\n";
		$query = $this->db->query($sql);
        if ($getTotalRecords == true) {
            $rc = $this->db->query("SELECT FOUND_ROWS()");
            $row = $rc->row_array();
            $total_row_count = $row['FOUND_ROWS()'];
        }
		$query_data = $query->result();
        if ($getTotalRecords == true) {

            return array(
                'total_row_count' => $total_row_count,
                'result' => $query_data,
            );
        }
		return $query_data;
	}
	function getCategoryDetails($id){
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_category') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getTotalImage($id){
		$this->db->where('classified_id', $id);
		$this->db->where('is_active','1');		
		$query = $this->db->get('tbl_files_image') or die(mysql_error());
//        echo $this->db->last_query();
		if($query->num_rows >= 1)
			return $query->result();
		else
            return false;
	}
	function getTotalImageCount($id){
		$this->db->where('classified_id', $id);
		$this->db->where('is_active','1');		
		$query = $this->db->get('tbl_files_image') or die(mysql_error());
		return $query->num_rows;
	}
	function getMemberDetails($id){
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_member') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getNewInquiryDetails($id){
		$this->db->where('classified_id', $id);
		$this->db->where('is_active', '0');		
		$query = $this->db->get('tbl_files_inquiry') or die(mysql_error());
		return $query->num_rows;
	}
	function getTotalInquiryDetails($id){
		$this->db->where('classified_id', $id);
		$query = $this->db->get('tbl_files_inquiry') or die(mysql_error());
		return $query->num_rows;
	}

    /**
     * Function to build sql query string for searching
     *
     * @param $searchCriteria array
     *
     * @return string
     */
    public function build_search_string($searchCriteria)
    {
        $searchString  = '';
        $keywordSearchString = '';
        if (isset($searchCriteria['country']) && $searchCriteria['country'] != '')
            $searchString .= ' AND classified_country = ' . $searchCriteria['country'];

        if (isset($searchCriteria['category']) && $searchCriteria['category'] != '' && is_numeric($searchCriteria['category']))
            $searchString .= ' AND category_id = ' . $searchCriteria['category'];

        if (isset($searchCriteria['sub_category']) && $searchCriteria['sub_category'] != '' && is_numeric($searchCriteria['sub_category']))
            $searchString .= ' AND sub_category_id = ' . $searchCriteria['sub_category'];

        if (isset($searchCriteria['brand_id']) && $searchCriteria['brand_id'] != '')
            $searchString .= ' AND brand_id = "' . $searchCriteria['brand_id'] . '"';

        if (isset($searchCriteria['model_id']) && $searchCriteria['model_id'] != '')
            $searchString .= ' AND model_id = "' . $searchCriteria['model_id'] . '"';

        if (isset($searchCriteria['fuel_type']) && $searchCriteria['fuel_type'] != '')
            $searchString .= ' AND fuel_type = "' . $searchCriteria['fuel_type'] . '"';

        if (isset($searchCriteria['manufacture_year_from']) && $searchCriteria['manufacture_year_from'] != '' && is_numeric($searchCriteria['manufacture_year_from']))
            $searchString .= ' AND manufacture_year >= ' . $searchCriteria['manufacture_year_from'];

        if (isset($searchCriteria['manufacture_year_to']) && $searchCriteria['manufacture_year_to'] != '' && is_numeric($searchCriteria['manufacture_year_to']))
            $searchString .= ' AND manufacture_year <= ' . $searchCriteria['manufacture_year_to'];

        if (isset($searchCriteria['min_amount']) && $searchCriteria['min_amount'] != '' && is_numeric($searchCriteria['min_amount']))
            $searchString .= ' AND amount >= ' . $searchCriteria['min_amount'];

        if (isset($searchCriteria['max_amount']) && $searchCriteria['max_amount'] != '' && is_numeric($searchCriteria['max_amount']))
            $searchString .= ' AND amount <= ' . $searchCriteria['max_amount'];

        if (isset($searchCriteria['search_city']) && $searchCriteria['search_city'] != '') {
            $searchString .= ' AND classified_city = "' . $searchCriteria['search_city'] . '"';
        }

        /*JOB*/
        if (isset($searchCriteria['career_level']) && $searchCriteria['career_level'] != '') {
            $searchString .= ' AND career_level = "' . $searchCriteria['career_level'] . '"';
        }

        if (isset($searchCriteria['experience_years']) && $searchCriteria['experience_years'] != '') {
            $searchString .= ' AND experience_years = "' . $searchCriteria['experience_years'] . '"';
        }

        if (isset($searchCriteria['education']) && $searchCriteria['education'] != '') {
            $searchString .= ' AND education = "' . $searchCriteria['education'] . '"';
        }

        if (isset($searchCriteria['employment_type']) && $searchCriteria['employment_type'] != '') {
            $searchString .= ' AND employment_type = "' . $searchCriteria['employment_type'] . '"';
        }
        /*JOB*/

        if (isset($searchCriteria['search_city']) && $searchCriteria['search_city'] != '') {
            $searchString .= ' AND classified_city = "' . $searchCriteria['search_city'] . '"';
        }

        $regionSearchString = '';
        if (isset($searchCriteria['search_region']) && $searchCriteria['search_region'] != '' && !is_numeric($searchCriteria['search_region'])) {
            $citySearchString = '';
            $localitySearchString = '';
            $regionIdSuggestion = $this->getAllRegionsFromString($searchCriteria['search_region']);
            $this->searchRegions = $regionIdSuggestion;
            if (!empty($regionIdSuggestion['city_result']) && is_array($regionIdSuggestion['city_result'])) {
                $city_List = '';
                foreach ($regionIdSuggestion['city_result'] as $city_result) {
                    $city_List .= (!empty($city_List) ? ', ' : '').$city_result->id;
                }
                if (!empty($city_List)) {
                    $citySearchString .= !empty($city_List) ? " classified_city IN ($city_List) " : '';
                }
            }
            if (!empty($regionIdSuggestion['locality_result'])) {
                $city_List = '';
                foreach ($regionIdSuggestion['locality_result'] as $city_result) {
                    $city_List .= (!empty($city_List) ? ', ' : '').$city_result->id;
                }
                if (!empty($city_List)) {
                    $localitySearchString .= !empty($city_List) ? " classified_locality IN ($city_List) " : '';
                }
            }
            $regionSearchString = $citySearchString;
            if (!empty($localitySearchString)) {
                $regionSearchString .= (!empty($regionSearchString) ? ' OR ' : '').$localitySearchString;
            }
        }
        $searchString .= !empty($regionSearchString) ? " AND ($regionSearchString)" : '';

        if (isset($searchCriteria['search_country']) && $searchCriteria['search_country'] != '')
            $searchString .= ' AND classified_country = "' . $searchCriteria['search_country'] . '"';

        if (isset($searchCriteria['classified_locality']) && $searchCriteria['classified_locality'] != '')
            $searchString .= ' AND classified_locality = "' . $searchCriteria['classified_locality'] . '"';

        if (isset($searchCriteria['transmission']) && $searchCriteria['transmission'] != '')
            $searchString .= ' AND transmission = "' . $searchCriteria['transmission'] . '"';

        if (isset($searchCriteria['career_level']) && $searchCriteria['career_level'] != '')
            $searchString .= ' AND career_level = "' . $searchCriteria['career_level'] . '"';

        if (isset($searchCriteria['experience_years']) && $searchCriteria['experience_years'] != '' && is_numeric($searchCriteria['experience_years']))
            $searchString .= ' AND experience_years >= ' . $searchCriteria['experience_years'] .' ';

        if (isset($searchCriteria['education']) && $searchCriteria['education'] != '')
            $searchString .= ' AND education = ' . $searchCriteria['education'] .' ';

        if (isset($searchCriteria['employment_type']) && $searchCriteria['employment_type'] != '')
            $searchString .= ' AND employment_type = ' . $searchCriteria['employment_type'] .' ';

        if (isset($searchCriteria['min_bed_rooms']) && $searchCriteria['min_bed_rooms'] != '' && is_numeric($searchCriteria['min_bed_rooms']))
            $searchString .= ' AND bedrooms >= ' . $searchCriteria['min_bed_rooms'];

        if (isset($searchCriteria['max_bed_rooms']) && $searchCriteria['max_bed_rooms'] != '' && is_numeric($searchCriteria['max_bed_rooms']))
            $searchString .= ' AND bedrooms <= ' . $searchCriteria['max_bed_rooms'];

        if (isset($searchCriteria['min_bath_rooms']) && $searchCriteria['min_bath_rooms'] != '' && is_numeric($searchCriteria['min_bath_rooms']))
            $searchString .= ' AND bathrooms >= ' . $searchCriteria['min_bath_rooms'];

        if (isset($searchCriteria['max_bath_rooms']) && $searchCriteria['max_bath_rooms'] != '' && is_numeric($searchCriteria['max_bath_rooms']))
            $searchString .= ' AND bathrooms <= ' . $searchCriteria['max_bath_rooms'];

        if (isset($searchCriteria['min_area_sqft']) && $searchCriteria['min_area_sqft'] != '' && is_numeric($searchCriteria['min_area_sqft']))
            $searchString .= ' AND area_sqft >= ' . $searchCriteria['min_area_sqft'];

        if (isset($searchCriteria['max_area_sqft']) && $searchCriteria['max_area_sqft'] != '' && is_numeric($searchCriteria['max_area_sqft']))
            $searchString .= ' AND area_sqft <= ' . $searchCriteria['max_area_sqft'];


        if (isset($searchCriteria['search_city']) && $searchCriteria['search_city'] != '' &&
            isset($searchCriteria['search_city_range']) && $searchCriteria['search_city_range'] != ''
        ) {
            $searchString .= ' AND (classified_city_distance <= ' . $searchCriteria['search_city_range'] .' OR classified_city_distance = 0 )';
        }

        if (isset($searchCriteria['search_keyword']) && $searchCriteria['search_keyword'] != '') {
            $keywordSearchString .= process_search($searchCriteria['search_keyword'], 'description', 'OR');
            $keywordSearchString .= process_search($searchCriteria['search_keyword'], 'small_description', 'OR');
            $keywordSearchString .= process_search($searchCriteria['search_keyword'], 'description', 'OR');
            $keywordSearchString .= process_search($searchCriteria['search_keyword'], 'classified_tags', 'OR');
            $keywordSearchString .= process_search($searchCriteria['search_keyword'], 'title');

            $searchString .= " AND ( $keywordSearchString )";
        }
        //echo $searchString;
        return $searchString;
    }

    function getAllRegionsFromString($search_region)
    {
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;

        $sql = "SELECT `id`, `title` FROM (`tbl_cities`) WHERE `title` LIKE '%$search_region%' AND is_active ='1' AND is_deleted = '0'  ORDER BY INSTR(`title`, '$search_region' ) ASC LIMIT 5";
        $query = $this->db->query($sql);

        $city_result = $query->result();

        $sql = "SELECT `id`, `title` FROM (`tbl_locality`) WHERE `title` LIKE '%$search_region%' AND is_active ='1' AND is_deleted = '0' ORDER BY INSTR(`title`, '$search_region' ) ASC LIMIT 5";
        $query = $this->db->query($sql);

        $locality_result = $query->result();

        return array(
            'city_result' => $city_result, 'locality_result' => $locality_result
        );
    }

    /**
     * Function to build pagination links
     *
     * @param $searchResultTotal
     * @param $per_page
     * @param $searchCriteria
     *
     * @return string
     */
    function build_search_paginator($searchResultTotal, $per_page, $searchCriteria, $base_url = '')
    {
        $this->load->library('pagination');

        unset($searchCriteria['per_page']);
        //$config['uri_segment'] = 2;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['base_url'] = !empty($base_url) ? $base_url : ROOT_URL.'/search?';
        $config['base_url'] .= (empty($searchCriteria) ? '' : http_build_query($searchCriteria));
        $config['total_rows'] = $searchResultTotal;
        $config['per_page'] = $per_page;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<span aria-hidden="true">&lt;&lt;</span>';;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<span aria-hidden="true">&lt;</span>';
        $config['prev_tag_open'] = '<li class="prev_li" >';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<span aria-hidden="true">&gt;</span>';
        $config['next_tag_open'] = '<li class="next_li">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active_li"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';

        $config['last_link'] = '<span aria-hidden="true">&gt;&gt;</span>';;
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    /**
     * Function to build search order query
     *
     * @param $searchCriteria
     *
     * @return string
     */
    public function build_search_order($searchCriteria)
    {
         $filters = array(
            1 => array(
                'string' => 'Posted Date ( Recent )',
                'query' => 'created_date_time DESC'
            ),
            2 => array(
                'string' => 'Price ( Lowest )',
                'query' => 'amount ASC'
            ),
            3 => array(
                'string' => 'Price ( Highest )',
                'query' => 'amount DESC'
            )
        );
        $order_by = (!empty($searchCriteria['order']) && is_numeric($searchCriteria['order'])) ? $searchCriteria['order'] : 1;
        $search_order['order_query'] = !empty($filters[$order_by]) ? $filters[$order_by]['query'] : $filters[1]['query'];
        $search_order['filters'] = $filters;
        if(isset($searchCriteria['order'])) { unset($searchCriteria['order']); }
        $search_order['sortUrl'] = ROOT_URL.'search?'.(empty($searchCriteria) ? '' : http_build_query($searchCriteria));

        return $search_order;
    }

    /**
     * Function to get distance from city options
     *
     * @return array
     */
    public function get_distance_from_city_options($searchCriteria)
    {
        if(isset($searchCriteria['search_city_range'])) { unset($searchCriteria['search_city_range']); }
        if(isset($searchCriteria['search_city'])) { unset($searchCriteria['search_city']); }

        $filter = array();
        for ($i = 10; $i <= 50; $i += 10) {
            $filter[$i] = "Within $i miles";
        }
        $distance_filter['url'] = ROOT_URL.'search?'.(empty($searchCriteria) ? '' : http_build_query($searchCriteria));
        $distance_filter['filter'] = $filter;

        return $distance_filter;
    }

    /**
     * Function to build url for keyword search
     *
     * @param $searchCriteria
     */
    public function get_keyword_search_url($searchCriteria)
    {
        if(isset($searchCriteria['keyword_search'])) { unset($searchCriteria['keyword_search']); }

        return ROOT_URL.'search?'.(empty($searchCriteria) ? '' : http_build_query($searchCriteria));
    }

    /**
     * Function to build search query string to be saved
     *
     * @param $searchCriteria
     *
     * @returnrn string
     */
    public function get_save_search_query_string($searchCriteria)
    {
        if(isset($searchCriteria['per_page'])) { unset($searchCriteria['per_page']); }
        if(isset($searchCriteria['order'])) { unset($searchCriteria['order']); }

        return (empty($searchCriteria) ? '' : urlencode(http_build_query(array_filter($searchCriteria))));
    }

    /**
     *
     * @param $searchCriteria
     */
    public function get_back_url_query_string($searchCriteria)
    {
        return (empty($searchCriteria) ? '' : base64_encode(http_build_query(array_filter($searchCriteria))));
    }

    public function getBackUrlParams($back)
    {
        //if (empty($back)) return array();
        //$backUrlParams = $this->getUrlParams(base64_decode($back));
        //print_r($backUrlParams);
        //parse_str($backUrlParams['path'], $output);

        //print_r($output);
    }


    /**
     * Function to set search page header
     *
     * @param $searchCriteria
     * @param $searchedCategory
     */
    public function set_search_page_header($searchCriteria, $searchedCategory, $regionSearch = '')
    {
        //Get category string
        $searched_category = (!$searchedCategory) ? '' : $searchedCategory->title;
        $searched_sub_category = !empty($searchCriteria['sub_category']) ? $this->getSubCategoryById($searchCriteria['sub_category']) : '';

        /*//Build page header string
        $search_page_header = ((empty($search_page_header) && !empty($searchCriteria['search_keyword'])) ? $searchCriteria['search_keyword'] : '');
        $search_page_header = (empty($search_page_header) && !empty($searchCriteria['sub_category'])) ? $searched_sub_category->title : $search_page_header;
        $search_page_header = empty($search_page_header) ? $searched_category : $search_page_header;
        $search_page_header = (empty($search_page_header) && !empty($searchCriteria['brand_id'])) ? $this->getBrandTitleById($searchCriteria['brand_id']) : $search_page_header;
        //if (empty($search_page_header) && !empty($searchCriteria['sub_category']) && !empty($searchCriteria['category']))

        $search_page_header = (empty($search_page_header) && !empty($searchCriteria['model_id'])) ? $this->getModelTitleById($searchCriteria['model_id']) : $search_page_header;

        $cityName = (!empty($searchCriteria['search_city'])) ? $this->getCityById($searchCriteria['search_city']) : '';
        $locality = (!empty($searchCriteria['classified_locality'])) ? $this->getLocalityById($searchCriteria['classified_locality']) : '';
        $location = '';

        if (!empty($search_page_header)) {
            if (!empty($locality) && !empty($cityName))
                $location =  ' in '.$locality.' / '.$cityName;
            else if (empty($locality) && !empty($cityName))
                $location =  ' in '.$cityName;
            else if (!empty($locality) && empty($cityName))
                $location =  ' in '.$locality;
        } else {
            if (!empty($locality) && !empty($cityName))
                $location =  $locality.' / '.$cityName;
            else if (empty($locality) && !empty($cityName))
                $location =  $cityName;
            else if (!empty($locality) && empty($cityName))
                $location =  $locality;
        }
        $sourcePath = '';
        if (!empty($_GET['source_catgories']) && $_GET['source_catgories'] ==1 ) {
            $sourcePath = '&source_catgories=1';
        } else if (!empty($_GET['source_locations']) && $_GET['source_locations'] ==1 ) {
            $sourcePath = '&source_locations=1';
        }
        $breadCrumbs = array();
        if (!empty($cityName)) {
            $breadCrumbs['city'] = array('id' => $searchCriteria['search_city'], 'title' => $cityName, 'url' => 'search_city='.$searchCriteria['search_city'].$sourcePath);
        }
        if (!empty($searchCriteria['classified_locality']) && is_numeric($searchCriteria['classified_locality'])) {
            $this->load->model('locality_model');
            $searchedLocality = $this->locality_model->getDetails($searchCriteria['classified_locality']);
            if (!empty($searchedLocality)) {
                $breadCrumbs['locality'] = array(
                    'id' => $searchedLocality->id, 'title' => $searchedLocality->title,
                    'url' => (!empty($breadCrumbs['city']['url']) ? $breadCrumbs['city']['url'] . '&' : '') . 'classified_locality=' . $searchedLocality->id.$sourcePath
                );
            }
        }
        if (!empty($searchedCategory)) {
            if (!empty($breadCrumbs['locality']['url'])){
                $url = $breadCrumbs['locality']['url'].'&' . 'category='.$searchedCategory->id;
            } else if (!empty($breadCrumbs['city']['url'])){
                $url = $breadCrumbs['city']['url'].'&' . 'category='.$searchedCategory->id;
            } else {
                $url = 'category='.$searchedCategory->id.$sourcePath;
            }
            $breadCrumbs['category'] = array(
                'id' => $searchedCategory->id, 'title' => $searchedCategory->title,
                'url' => $url.$sourcePath
            );
        }
        if (!empty($searched_sub_category)) {
            if (!empty($breadCrumbs['category']['url'])){
                $url = $breadCrumbs['category']['url'].'&' . 'sub_category='.$searched_sub_category->id;
            } else if (!empty($breadCrumbs['locality']['url'])){
                $url = $breadCrumbs['locality']['url'].'&' . 'sub_category='.$searched_sub_category->id;
            } else if (!empty($breadCrumbs['city']['url'])){
                $url = $breadCrumbs['city']['url'].'&' . 'sub_category='.$searched_sub_category->id;
            } else {
                $url = 'sub_category='.$searched_sub_category->id.$sourcePath;
            }
            $breadCrumbs['sub_category'] = array(
                'id' => $searched_sub_category->id, 'title' => $searched_sub_category->title,
                'url' => $url
            );
        }*/
        //
        $location = '';
        $breadCrumbs = '';
        $search_page_header = '';

        if (!empty($searchCriteria['classified_locality'])) {
            if (!empty($search_page_header)) {
                $search_page_header .= ', ';
            }
            $cityName = $this->getLocalityById($searchCriteria['classified_locality']);
            //$searchCriteria['search_country']
            $search_page_header .= !empty($cityName) ? " in $cityName" : '';
        }

        if (!empty($searchCriteria['search_city'])) {
            if (!empty($search_page_header)) {
                $search_page_header .= ', ';
            } else {
                $search_page_header .= ' in ';
            }
            $cityName = $this->getCityById($searchCriteria['search_city']);
            //$searchCriteria['search_country']
            $search_page_header .= !empty($cityName) ? " $cityName" : '';
        }
        if (!empty($searchCriteria['search_country'])) {
            if (!empty($search_page_header)) {
                $search_page_header .= ', ';
            } else {
                $search_page_header .= ' in ';
            }
            $countryName = $this->getCountryById($searchCriteria['search_country']);
            //$searchCriteria['search_country']
            $search_page_header .= !empty($countryName) ? " <span>$countryName</span> " : '';
        }
        if (!empty($regionSearch) && !empty($regionSearch['city_result']) && is_array($regionSearch['city_result'])) {
            $search_page_header .= !empty($search_page_header) ? ', ' : '';
            $search_page_header .= 'in city(s) ';
            $cityList = '';
            foreach ($regionSearch['city_result'] as $city) {
                $cityList .= (!empty($cityList) ? ', ' : '').'<span>'.$city->title.'</span>';
            }
            $search_page_header .= $cityList;
        }
        if (!empty($regionSearch) && !empty($regionSearch['locality_result']) && is_array($regionSearch['locality_result'])) {
            if (!empty($cityList)) {
                $search_page_header .= ' or in Locality(s) ';
            } else if (!empty($search_page_header)) {
                $search_page_header .= ', ';
            }

            $cityList = '';
            foreach ($regionSearch['locality_result'] as $city) {
                $cityList .= (!empty($cityList) ? ', ' : '').'<span>'.$city->title.'</span>';
            }
            $search_page_header .= $cityList;
        }
        if (!empty($searched_category)) {
            $search_page_header .= !empty($search_page_header) ? ', ' : '';
            $search_page_header .= !empty($searched_category) ? ' under the category <span>'.$searched_category.'</span>' : '';
        }
        return array( 0 => $search_page_header . $location, 1 => $breadCrumbs);
    }

    public function getModelTitleById($id)
    {
        $modelDetails = $this->model_model->getDetails($id);

        return !empty($modelDetails->title) ? $modelDetails->title : '';
    }

    public function getBrandTitleById($id)
    {
        $modelDetails = $this->brand_model->getDetails($id);

        return !empty($modelDetails->title) ? $modelDetails->title : '';
    }


    public function getCityById($id)
    {
        $this->load->model('admin_model');
        $result = $this->admin_model->getCity($id);
        $city = !empty($result[0]) ? $result[0] : null;

        return !empty($city->title) ? $city->title : '';
    }


    public function getCountryById($id)
    {
        $this->load->model('admin_model');
        $result = $this->admin_model->getCountryById($id);
        $city = !empty($result[0]) ? $result[0] : null;

        return !empty($city->name) ? $city->name : '';
    }
    public function getLocalityById($id)
    {
        $this->load->model('locality_model');
        $result = $this->locality_model->getAllRecords('id, title', " id='$id' ");
        $locality = !empty($result[0]) ? $result[0] : null;

        return !empty($locality->title) ? $locality->title : '';
    }
    public function getSubCategoryById($id)
    {
        $this->load->model('category_model');
        $result  = $this->category_model->getDetails($id);

        return !empty($result) ? $result : '';
    }


    /**
     * Function to validate member registration form
     *
     * @return boolean
     */
    public function validate_add()
    {
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        //$this->form_validation->set_rules('title', 'Title', 'xss_clean|trim|required');
        $this->form_validation->set_rules('uploaded_files', 'File', 'xss_clean|required|callback_file_exists');
        //registration_ip
        return $this->form_validation->run();
    }

    function move_cropped_files() {
        $fileList = $_POST['uploaded_files'];
        $titleList = $_POST['uploaded_title'];

        $movedFileList = false;
        if (!empty($fileList) && is_array($fileList)) {
            foreach ($fileList as $key => $file) {
                if (!empty($file) && file_exists(TEMP_DIR_UPLOAD_FILE.$file) && !is_dir(TEMP_DIR_UPLOAD_FILE.$file)) {
                    //echo "<br /><br /> $key => ".$file;
                    $target_path_parts = pathinfo($file);

                    $new_file_name = $target_path_parts['filename'] . '.' . $target_path_parts['extension'];
                    $i = 1;
                    //echo "<br /><br />".$new_file_name;
                    while (file_exists(DIR_UPLOAD_FILE . $new_file_name)) {
                        $new_file_name = $target_path_parts['filename'] . '_' . ($i++) . '.' . $target_path_parts['extension'];
                    }
                    //echo "<br /><br />".$new_file_name;
                    $movedFileList[$key] = array(
                        'title' => !empty($titleList[$key]) ? $titleList[$key] : 'Source File',
                        'file' => $new_file_name
                    );
                    rename(TEMP_DIR_UPLOAD_FILE.$file, DIR_UPLOAD_FILE.$new_file_name);
                    chmod_apply(DIR_UPLOAD_FILE.$new_file_name);
                }
            }
        }
//var_dump($movedFileList);
        return $movedFileList;

        print_r($files);
        $titles = $_POST['uploaded_title'];
        print_r($titles);
        die();
        $new_file_name = false;
        if (!empty($cropped_file) && file_exists(ROOT_DIRECTORY.$cropped_file) && !is_dir(ROOT_DIRECTORY.$cropped_file)) {
            $target_path_parts = pathinfo($cropped_file);

            $new_file_name = $target_path_parts['filename'] . '.' . $target_path_parts['extension'];
            $i = 1;

            while (file_exists(DIR_UPLOAD_BANNER . $new_file_name)) {
                $new_file_name = $target_path_parts['filename'] . '-' . ($i++) . '.' . $target_path_parts['extension'];
            }

            rename(ROOT_DIRECTORY.$cropped_file, DIR_UPLOAD_BANNER.$new_file_name);
            chmod_apply(DIR_UPLOAD_BANNER.$new_file_name);
        } else {
            //echo "<br />".ROOT_DIRECTORY.$cropped_file;
        }
        //echo "<br />New file moved zz: ".$new_file_name;
        print_r($_POST); die("Here");
        return $new_file_name;
    }


    /**
     * Function to save combination of category and brands searches
     * This data can be used for displaying popular searches
     *
     * @param $searchCriteria
     */
    public function saveCategoryBrandSearch($searchedCategory)
    {
        if ($this->input->get('brand_id') && $this->input->get('category')) {
            $brand = $this->brand_model->getDetailsFromTitle($this->input->get('brand_id'));
            if (!empty($brand->id)) {
                $this->db->select('id');
                $this->db->where('category_id', $searchedCategory->id);
                $this->db->where('brand_id', $brand->id);
                $query = $this->db->get('tbl_category_brand_search');
                if ($query->num_rows >= 1) {
                    //Update
                    $result = $query->row();
                    $this->db->where('id', $result->id);
                    $this->db->set('search_count', 'search_count+1', FALSE);
                    $this->db->set('search_string', $searchedCategory->sub_title . ' ' . $brand->title);
                    $this->db->set('brand_title', $brand->title);
                    $this->db->update('tbl_category_brand_search');
                } else {
                    $data = array(
                        'category_id' => $searchedCategory->id,
                        'brand_id' => $brand->id,
                        'search_string' => $searchedCategory->sub_title . ' ' . $brand->title,
                        'brand_title' => $brand->title,
                        'created_date_time' => date('Y-m-d H:i:s'),
                        'search_count' => 1
                    );
                    //Insert
                    $this->db->insert('tbl_category_brand_search', $data);
                }
            }
        }
    }

    public function getTopSearch()
    {
        $this->db->order_by('search_count', 'DESC');
        $this->db->limit(10);
        $query = $this->db->get('tbl_category_brand_search');

        return $query->result();
    }

    /**
     * Function to upload classified imnages
     *
     * @param $classifieds_id
     * @param $file_input_name
     *
     * @return array
     */
    public function save_classifie_images($classifieds_id, $file_input_name)
    {
        $uploaded_file_info = false;
        if (!empty($_FILES[$file_input_name])) {
            $number_of_images = $this->get_number_of_images_uploaded($_FILES[$file_input_name]);
            if ($number_of_images > 0) {
                $config['upload_path'] = DIR_UPLOAD_CLASSIFIED;
                //$config['allowed_types'] = array('gif', 'jpg', 'jpeg', 'jpe', 'png');
                $uploadedFileList = $_FILES[$file_input_name]['name'];
                $uploadedFileList = array_slice($uploadedFileList, 0, 5, true);

                foreach ($uploadedFileList as $key => $uploaded_file_name) {
                    if (!empty($uploaded_file_name)) {
                        $uploaded_path_parts = pathinfo($uploaded_file_name);
                        $temp_name = $_FILES[$file_input_name]['tmp_name'][$key];
                        //var_dump(sprintf("%06d",rand()));
                        $fileName = $classifieds_id . uniqid('', true) . "-" . date("YmdHis") . "." . sprintf("%06d", rand());
                        $fileFullName = $fileName . "." . $uploaded_path_parts['extension'];
                        $target_path_parts = pathinfo($fileName);
                        $target_file_name = $target_path_parts['filename'] . '.' . $uploaded_path_parts['extension'];
                        $i = 1;
                        while (file_exists($config['upload_path'] . $target_file_name)) {
                            $target_file_name = $target_path_parts['filename'] . '-' . ($i++) . '.' . $uploaded_path_parts['extension'];
                        }
                        $config['file_name'] = $target_file_name;
                        $files_info = $this->move_uploaded_file_to_location($temp_name, $target_file_name, $config);

                        $uploaded_file_info[] = array(
                            'target_name' => $target_file_name,
                        );
                    }
                }
            }
            return $uploaded_file_info;
        }
    }

    /**
     * Function to get number of images uploaded
     *
     * @param array $image_uploader_multiple
     *
     * @return number
     */
    function get_number_of_images_uploaded($image_uploader_multiple)
    {
        $count = 0;
        if (isset($image_uploader_multiple['error']) && is_array($image_uploader_multiple['error'])) {
            foreach($image_uploader_multiple['error'] as $error) {
                if ($error != 4) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * function to move uploaded file to specific location
     *
     * @param string $temp_name
     * @param string $target_file_name
     * @param array $config
     *
     * @return multitype:string unknown multitype:
     */
    function move_uploaded_file_to_location($temp_name, $target_file_name, $config)
    {
        error_reporting(E_ALL);

        move_uploaded_file($temp_name, $config['upload_path'].$target_file_name);
        $this->chmod_apply($config['upload_path'].$target_file_name);

        return array(
            'full_path' => $target_file_name
        );

        /*$temp_name = str_replace('\\', '/', $temp_name);
        error_reporting(E_ALL);

        $fileTarget = $config['upload_path'].$target_file_name;
        $fileTarget = str_replace('/', '\\', $fileTarget);

        echo "<br /><br />";
        print_r(array($temp_name, $config['upload_path'].$target_file_name, $fileTarget));
        echo "<br /><br />";

        move_uploaded_file($temp_name, $fileTarget);
        $this->chmod_apply($fileTarget);

        return array(
            'full_path' => $target_file_name
        );*/
    }

    /**
     * Function to apply permission to the upload file
     *
     * @param $filename
     * @return bool
     */
    function chmod_apply($filename = '') {
        $stat = @ stat(dirname($filename));
        $perms = $stat['mode'] & 0007777;
        $perms = $perms & 0000666;
        if ( @chmod($filename, $perms) )
            return true;
        return false;
    }

    /**
     * Function to get total records count
     *
     * @return mixed
     */
    function getAllRecordsCount()
    {
        $rc = $this->db->query("SELECT FOUND_ROWS()");
        $row = $rc->row_array();
        $row_count = $row['FOUND_ROWS()'];

        return $row_count;
    }

    function prepareClassifiedSearch($classified_slug, $back)
    {
        $data['classifiedDetails'] = $this->getFullDetailsBySlug($classified_slug);

        $data['classifiedDetails']->city = $this->getCityById($data['classifiedDetails']->classified_city);
        $data['classifiedDetails']->locality = $this->getLocalityById($data['classifiedDetails']->classified_locality);

        $data['classifiedImages'] = !empty($data['classifiedDetails']->id) ? $this->getTotalImage($data['classifiedDetails']->id) : '';
        $data['backUrl'] = ROOT_URL .'search'. !empty($back) ? '?'.base64_decode($back) : '';

        return $data;
    }

    function getFullDetailsBySlug($classified_slug){
        $this->db->select('clfd.*, categ.title AS categoryTitle, tvb.title AS brand, tvm.title AS model, tvf.title AS fuel');
        $this->db->from('tbl_files AS clfd');
        $this->db->JOIN('tbl_category AS categ', 'categ.id=clfd.category_id', 'left');
        $this->db->JOIN('tbl_vehicle_brand AS tvb', 'tvb.id=clfd.brand_id', 'left');
        $this->db->JOIN('tbl_vehicle_model AS tvm', 'tvm.id=clfd.model_id', 'left');
        $this->db->JOIN('tbl_fuel_type AS tvf', 'tvf.id=clfd.fuel_type ', 'left');

        $this->db->where('clfd.is_deleted', '0');
        $this->db->where('clfd.classified_slug', $classified_slug);
        $query = $this->db->get();//'tbl_files') or die(mysql_error());

        if($query->num_rows >= 1) {
            $result = $query->row();
        } else {
            $result = false;
        }

        return $result;
    }

    function getSearchCriteriaFromBack($back)
    {
        $queryParts = array();
        $searchUrl = base64_decode($back);
        if (!empty($searchUrl)) {
            $searchUrl = ROOT_URL . '?' . $searchUrl;
            $searchUrl = !empty($searchUrl) ? urldecode($searchUrl) : '';
            $parsed_url = parse_url($searchUrl);
            $query = html_entity_decode($parsed_url['query']);
            if (!empty($query)) {
                parse_str($query, $queryParts);
            }
        }

        return $queryParts;
    }

    function getClassifiedsCount($status){

        $sql ="select count(id) as total_classified FROM tbl_files WHERE is_active = '$status' AND is_deleted = '0' ";

        $query = $this->db->query($sql);
        $result = $query->result();

        return $result[0]->total_classified;

    }

    function updateFileConversionStatus($userId, $initial_status, $final_status) {
        $this->db->where("created_by", $userId);
        $this->db->where("file_status", $initial_status);
        $data = array("file_status" => $final_status);
        $this->db->update('tbl_files',$data);

        //echo "\n\n".$this->db->last_query(). "\n\n";
    }

    function getAllFileList($file_owner_id = '') {
        $sql ="select tbl_files.*, tbl_files.created_date_time, concat(tbl_member.first_name, ' ',  tbl_member.last_name ) as member_name FROM tbl_files
                LEFT JOIN tbl_member ON tbl_member.id = tbl_files.created_by
              WHERE 1=1 ";
        $sql .= !empty($file_owner_id) ? " AND tbl_files.created_by = $file_owner_id " : '';
        $sql .= " AND tbl_files.is_deleted='0' ORDER BY created_date_time DESC";
        $query = $this->db->query($sql);
        $query_data = $query->result();

        return $query_data;
    }

    function getAllRecordCount($where=''){
        $sql ="select COUNT(*) AS record_count FROM tbl_files WHERE is_deleted = '0' ";
        if($where!=''){
            $sql .= " AND $where ";
        }
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $query_data = $query->row();

        return !empty($query_data->record_count) ? $query_data->record_count : 0;
    }
}