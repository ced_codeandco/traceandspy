<?php
class Home_page_image_model extends CI_Model {

    var $image_title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		
		$order = $this->getLastOrder();
		$home_page_image_order = $order + 1;
		
		$data = array(
			'image_title' => $this->input->post('image_title'),
			'image_title_2' => $this->input->post('image_title_2'),			
			'image_path' => $this->input->post('image_path'),
			'home_page_image_order' => $home_page_image_order,
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_homepage_image',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		
		$data = array(
			'image_title' => $this->input->post('image_title'),
			'image_title_2' => $this->input->post('image_title_2'),
			'image_path' => $this->input->post('image_path'),
			'is_active' => $this->input->post('is_active')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_homepage_image',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_homepage_image SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_homepage_image WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_homepage_image') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_homepage_image WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		return $query_data;
	}
	
	function getLastOrder(){
		$sql ="select home_page_image_order FROM tbl_homepage_image ORDER BY home_page_image_order desc LIMIT 0,1 "; 
		$query = $this->db->query($sql);
		$result = $query->result();
		if(count($result) > 0)
			return $result[0]->home_page_image_order;	
		else
			return 0;
	}
	
	function changeOrder($id,$home_page_image_order,$position){

		if($id!='' && $home_page_image_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);	
			if($position=='Dn'){
				$qr="select home_page_image_order,id from tbl_homepage_image where home_page_image_order > '".$home_page_image_order."' order by home_page_image_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->home_page_image_order;
					$qry = "UPDATE tbl_homepage_image SET `home_page_image_order`= $home_page_image_order WHERE id =".$NewId;
						mysql_query($qry);

					$qry1 = "UPDATE tbl_homepage_image SET `home_page_image_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select home_page_image_order,id from tbl_homepage_image where home_page_image_order < '".$home_page_image_order."' order by home_page_image_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->home_page_image_order;
					$qry = "UPDATE tbl_homepage_image SET `home_page_image_order`= $home_page_image_order WHERE id =".$NewId;
					mysql_query($qry);

					$qry1 = "UPDATE tbl_homepage_image SET `home_page_image_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
		}		
	}
	
	
}